import { useEffect, useState } from "react";
import axios from "../api/dataAxios";

export default function useFetchProdBySeller(query) {
  const [dataProduct, setData] = useState([]);
//   console.log(dataProduct);
  const [status, setStatus] = useState(false);
  const [IMGs, setIMGs] = useState([]);
  const token = localStorage.getItem('token');
  var tokenWithoutQuotes = "";
  if (token!=null){
    tokenWithoutQuotes = token.replace(/"/g, '');
  }
  useEffect(() => {
    axios
      .get("seller_products", {
        headers: {
          "Authorization": `Bearer ${tokenWithoutQuotes}`,
          "Content-Type": "application/json"
        }
      })
      .then((e) => {
        // console.log(e);
        let DATA = e.data.products;
        // console.log(DATA);
        // DATA.images = [];
        setData(DATA);
        {
            DATA.map((product)=>{
                product.thumbnail = ""
                // console.log(product);
                axios.get("list_files", {
                    params: {
                    product_id: product.id,
                    user_id: 123,//will add token here in the future
                    },
                }).then((image)=>{
                    var val = ""
                    if(image!=null)
                    {
                        val = image.data[0]
                        // console.log(val);
                        const filePath =
                            "http://127.0.0.1:8080/get_file?product_id=" +
                            product.id +
                            "&file_name=" +
                            val +
                            "&user_id=" +
                            product.seller_id;
                        product.thumbnail = filePath;
                    }
                    else{
                        const filePath = "";
                        product.thumbnail = "";
                    }
                })
            })
        }
        
          if (e.status === 200) {
              // console.log(DATA);
            //   console.log(dataProduct);
              setData(DATA);
              // console.log(dataProduct);
              setStatus(true);
            } else {
              setStatus(false);
              setData([]);
            }
          });
  }, [query]);

  return {
    dataProduct,
    status,
  };
}
