import { useEffect, useState } from "react";
import axios from "../api/dataAxios";

export default function useFetchAllProd() {
    const [data, setData] = useState([]);
    const [status, setStatus] = useState(false);
    console.log("here")
    useEffect(()=>{
        axios.get("products").then((e)=>{
            let DATA = e.data
            DATA.products.map((key, val)=>{
                DATA.products[val].images = []
                // console.log(DATA.products[val])
                axios.get("list_files", {params: {product_id: DATA.products[val].id, user_id: DATA.products[val].seller_id}}).then((images)=>{
                    const imagess = []
                    {images.data.map((fname) => {
                        const filePath = "http://127.0.0.1:8080/get_file?product_id=" + DATA.products[val].id + "&file_name="+fname+"&user_id="+DATA.products[val].seller_id;
                        // console.log(filePath)
                        imagess.push(filePath)
                    })}
                    DATA.products[val].thumbnail = imagess[0]
                    DATA.products[val].images = imagess
                })
            // console.log(DATA)
        })
        if(e.status === 200){
            setStatus(true);
            setData(DATA.products);
        }
        else{
            setStatus(false);
            setData([]);
        }
    })
    }, []);

    return{
        data,
        status
    };

};