import { useState } from "react";

export default function useToken() {
  const getToken = () => {
    const tokenString = localStorage.getItem("token");
    const userToken = JSON.parse(tokenString);
    return userToken;
  };

  const getUserName = () => {
    const userNameString = localStorage.getItem("userName");
    const userName = JSON.parse(userNameString);
    return userName;
  };

  const [token, setToken] = useState(getToken());
  const [userName, setUserName] = useState(getUserName());

  const saveToken = (userToken) => {
    localStorage.setItem("token", JSON.stringify(userToken));
    setToken(userToken);
  };

  const saveName = (userName) => {
    localStorage.setItem("userName", JSON.stringify(userName));
    setUserName(userName);
    console.log("saved UserName: ", userName);
  };

  return {
    setToken: saveToken,
    token,
    setUserName: saveName,
    userName,
  };
}
