import { useEffect, useState } from "react";
import axios from "../api/dataAxios";

export default function useFetchData(query) {
  const [dataProduct, setData] = useState(null);
  const [status, setStatus] = useState(false);
  const [IMGs, setIMGs] = useState([]);

  useEffect(() => {
    axios
      .get("products", {
        params: {
          id: query,
        },
      })
      .then((e) => {
        let DATA = e.data.product;
        // console.log(DATA);
        DATA.images = [];
        setData(DATA);
        axios
          .get("list_files", {
            params: {
              product_id: e.data.product.id,
              user_id: e.data.product.seller_id,
            },
          })
          .then((images) => {
            const imagess = [];
            {
              images.data.map((val) => {
                // console.log("in usefetchdata val = ", val)
                const filePath =
                  "http://127.0.0.1:8080/get_file?product_id=" +
                  e.data.product.id +
                  "&file_name=" +
                  val +
                  "&user_id=" +
                  e.data.product.seller_id;
                // console.log(filePath)
                imagess.push(filePath);
              });
            }
            DATA.images = imagess;
            if (e.status === 200) {
              console.log(DATA);
              setData(DATA);
              setStatus(true);
            } else {
              setStatus(false);
              setData(null);
            }
          });
      });
  }, [query]);

  return {
    dataProduct,
    status,
  };
}
