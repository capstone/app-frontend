import { useEffect, useState } from "react";
import axios from "../api/dataAxiosOld";

export default function useFetchData(query) {
    const [data, setData] = useState(null);
    const [status, setStatus] = useState(false);

    useEffect(()=>{
        axios.get().then((e)=>{
            if(e.status === 200){
                setStatus(true);
                setData(e.data);
            }
            else{
                setStatus(false);
                setData(null);
            }
        })
    }, [query]);

    return{
        data,
        status
    };

};