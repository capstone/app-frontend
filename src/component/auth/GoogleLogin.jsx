import "./googleLogin.css";
import { FcGoogle } from "react-icons/fc";
import { GoogleAuthProvider, signInWithPopup } from "firebase/auth";
import { auth } from "../../utils/firbase";

function GoogleLogin() {
  //Google->Sign in
  var googleProvider = new GoogleAuthProvider();
  googleProvider.setCustomParameters({
    hd: "vt.edu",
  });
  googleProvider.addScope("email");
  const GoogleLogin = async () => {
    try {
      const result = await signInWithPopup(auth, googleProvider);
      console.log(result.user);
      if (result.user.email.split("@")[1] !== "vt.edu") {
        console.log("Please use @vt.edu");
      } else {
        console.log("welcome ", result.user.displayName);
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <button className="GoogleLogin" onClick={GoogleLogin}>
      <FcGoogle />&nbsp;
      Google Login
    </button>
  );
}

export default GoogleLogin;
