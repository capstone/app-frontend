// CheckPoint
import "./productCard.css";
import { useNavigate } from "react-router-dom";
import axios from "../../api/dataAxios";
import { useEffect, useState } from "react";
import 'font-awesome/css/font-awesome.min.css';
import { FacebookShareButton } from 'react-share';
import { WhatsappShareButton } from 'react-share';
import ShareButton from "react-share/lib/ShareButton";

function ProductCard({ seller_id, name, price, date, id, edit, deletePrd, markAsSold, is_sold}) {

  const navigate = useNavigate();
  const [productThumbnail, setProductThumbnail] = useState();
  
  const shareURL = "http://127.0.0.1:3000/product/" + id;
  const quote = 'Your quote goes here';
  const tweetUrl = `https://twitter.com/intent/tweet?url=${encodeURIComponent(shareURL)}`;
  // create a new Date object for today's date
  const today = new Date();
  const marketAvailableDate = new Date(date);

  const clickView = () => {
    navigate("/product/"+id);
  };
  
  const clickEdit = () => {
    navigate("/editproduct/"+id);
  };

  const clickMarkAsSold = () => {
    if (window.confirm("Are you sure you want to mark this product as sold?")) {
      const token = localStorage.getItem('token')
      var tokenWithoutQuotes = "";
      if (token!=null){
        tokenWithoutQuotes = token.replace(/"/g, '');
      }
      axios.put('/product_mark_sold', {
        id: id,
        is_sold: true
      }, {
      headers: {
        Authorization: `Bearer ${tokenWithoutQuotes}`,
      },
      })
      .then((response) => {
        console.log(response.data.message); // prints 'mark_product_sold() product updated and marked as sold successfully'
        window.location.reload();
      })
      .catch((error) => {
        console.log(error.response.data.message);
      });
    }
  };

  const clickDelete = () => {
    if (window.confirm("Are you sure you want to delete this product?")) {
      const token = localStorage.getItem('token')
      var tokenWithoutQuotes = "";
      if (token!=null){
        tokenWithoutQuotes = token.replace(/"/g, '');
      }
      axios.delete(`/product/${id}`, {
      headers: {
        Authorization: `Bearer ${tokenWithoutQuotes}`,
      },
    })
    .then((response) => {
      console.log(response.data.message); // prints 'Product deleted successfully'
      window.location.reload();
    })
    .catch((error) => {
      console.log(error.response.data.message); // prints 'You are not authorized to delete this product' or 'Product not found'
    });
    }
  };
  
  console.log("id - ", id, " seller_id: ", seller_id)
  
  axios.get("list_files", {params: {product_id: id, user_id: seller_id}}).then((images)=>{
    // console.log(images)
    // console.log("http://127.0.0.1:8080/get_file?product_id=" + id + "&file_name="+images.data[0]+"&user_id="+seller_id);
    setProductThumbnail("http://127.0.0.1:8080/get_file?product_id=" + id + "&file_name="+images.data[0]+"&user_id="+seller_id)
  })

  const handleFBClick = () => {
    window.open(`https://www.facebook.com/sharer/sharer.php?u=${shareURL}`);
  };

  const handleTwitterClick = () => {
    window.open(`https://www.facebook.com/sharer/sharer.php?u=${shareURL}`);
  };

  const handleWhatsappClick = () => {
    window.open(`https://wa.me/?text=${shareURL}`);
  };
  
  if(is_sold){
    return (
      <div className="ProductCard">
        <div className="img">
          <img src={productThumbnail} className="ProductImage" />
          <div class="soldOverlay">
            <p id="SoldOverlayText">SOLD!</p>
          </div>
        </div>
        <div className="ProductInfo">
          <button className="View" onClick={clickView}>
            View
          </button>
          <p className="ProductName">{name}</p>
          <p className="ProductPrice">Price - $ {price}</p>
          <p className="DateOfPost">Available On - {date}</p>
        </div>
      </div>
    );

  }
  else if(marketAvailableDate > today){
    return(
    <div className="ProductCard">
        <div className="img">
          <img src={productThumbnail} className="ProductImage" />
          <div class="FutureAvailable">
            <p id="FutureAvailableOverlayText">Future Available!</p>
          </div>
        </div>
        <div className="ProductInfo">
          <button className="View" onClick={clickView}>
            View
          </button>
          <p className="ProductName">{name}</p>
          <p className="ProductPrice">Price - $ {price}</p>
          <p className="DateOfPost">Available On - {date}</p>
          <div>
            <a onClick={handleFBClick}>
              <i class="fa fa-facebook-f"></i>
            </a>
            <a href={tweetUrl} target="_blank" rel="noopener noreferrer">
              <i class="fa fa-twitter"></i>
            </a>
            <a onClick={handleWhatsappClick}>
              <i class="fa fa-whatsapp"></i>
            </a>
          </div>
        </div>
      </div>
      );
  }
  else{
    return (
      <div className="ProductCard">
        <div className="img">
          <img src={productThumbnail} className="ProductImage" />
        </div>
        <div className="ProductInfo">
          <button className="View" onClick={clickView}>
            View
          </button>
          <div>
            {edit && <button className="Edit" onClick={clickEdit}>
              edit
            </button>}
            {deletePrd && <button className="Delete" onClick={clickDelete}>
              delete
            </button>}
          </div>
          {markAsSold && <button className="MarkAsSold" onClick={clickMarkAsSold}>
            Mark as Sold
          </button>}
          <p className="ProductName">{name}</p>
          <p className="ProductPrice">Price - $ {price}</p>
          <p className="DateOfPost">Available On - {date}</p>
          <div>
            <a onClick={handleFBClick}>
              <i class="fa fa-facebook-f"></i>
            </a>
            <a href={tweetUrl} target="_blank" rel="noopener noreferrer">
              <i class="fa fa-twitter"></i>
            </a>
            <a onClick={handleWhatsappClick}>
              <i class="fa fa-whatsapp"></i>
            </a>
          </div>
        </div>
      </div>
    );
  }
  
  
}

export default ProductCard;
