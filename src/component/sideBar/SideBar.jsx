import { slide as Menu } from "react-burger-menu";
import "./sideBar.css";
import { useAuthState } from "react-firebase-hooks/auth";
import { auth } from "../../utils/firbase";
import GoogleLogin from "../auth/GoogleLogin";

function SideBar() {
  const [user, loading] = useAuthState(auth);
  return (
    <Menu>
      <a className="menu-item" href="/all-products">
        Buyer
      </a>
      <a className="menu-item" href="/seller">
        Seller
      </a>
      <a className="menu-item" href="/about">
        About Us
      </a>
      <a className="menu-item" href="/contact">
        Contact Us
      </a>
      {!user && <GoogleLogin />}
      {user && (
        <div className="logOutOption">
          <button className="GoogleLogin" onClick={() => auth.signOut()}>
            Sign out
          </button>
        </div>
      )}
    </Menu>
  );
}

export default SideBar;
