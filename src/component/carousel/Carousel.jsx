import "./carousel.css";
import { FaArrowAltCircleRight, FaArrowAltCircleLeft } from "react-icons/fa";
import {AiFillCloseCircle, AiFillCheckCircle} from "react-icons/ai"
import { useState } from "react";

function Carousel({ SRC, edit, setRemoved, removed }) {
  // console.log(SRC.length);
  // console.log("here")
  if(edit==null){
    edit = false;
    removed = []
  }
  const [currImage, setCurrImage] = useState(0);
  // const [removed, setRemoved] = useState([])
  const prevSlide = () => {
    if (currImage - 1 < 0) {
      setCurrImage(SRC.length - 1);
    } else {
      setCurrImage(currImage - 1);
    }
  };

  const nextSlide = () => {
    if (currImage + 1 === SRC.length) {
      setCurrImage(0);
    } else {
      setCurrImage(currImage + 1);
    }
  };

  const removeImage = () => {
    setRemoved(removed => [...removed,currImage] )
    console.log(removed)
    nextSlide();
  }

  const includeImage = () => {
    setRemoved((removed) => removed.filter((val, _)=>val!==currImage));
    console.log(removed);
    nextSlide();
  }
  // SRC.map((s, i) => console.log("s=", s, " i=", i));
  return (
    <div className="productImg">
      <FaArrowAltCircleLeft className="leftArrow" onClick={prevSlide} />
      {edit && !removed.includes(currImage) && <AiFillCloseCircle className="close" onClick={removeImage}/>}
      {edit && removed.includes(currImage) && <AiFillCheckCircle className="close" onClick={includeImage}/>}
      {SRC.map((source, index) => {
        return (
          <img
            src={SRC[index]}
            className="ProductImage"
            key={index}
            style={{ display: currImage == index ? "flex" : "None", opacity: removed.includes(currImage) ? "0.2" : "1"}}
          />
        );
      })}
      <FaArrowAltCircleRight className="rightArrow" onClick={nextSlide} />
    </div>
  );
}

export default Carousel;
