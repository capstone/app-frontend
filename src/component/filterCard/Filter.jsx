import "./filter.css";
import { useEffect, useState } from "react";
import Slider from "@mui/material/Slider";
import ReactSlider from 'react-slider';

function Filter(data, setVisData) {

  const[CAT, setCat] = useState(["All"]);
  const [LOC, setLOC] = useState(["All"]);
  const [maxPrice, setMaxPrice] = useState(0);
  const[price, setPrice] = useState(0);

  var max_price=0;
  
  useEffect(()=>{
    console.log(data)
    data.data.map((e)=>{
      if(!LOC.includes(e.location)){
        console.log("here");
        setLOC(LOC => {
          if (!LOC.includes(e.location)) {
            return [...LOC, e.location];
          } else {
            return LOC;
          }
        });
      }
      if(!CAT.includes(e.category)){
        console.log("here");
        setCat(CAT => {
          if (!CAT.includes(e.category)) {
            return [...CAT, e.category];
          } else {
            return CAT;
          }
        });
        
      }
      if(max_price<e.price){
        max_price = e.price;
      }
      console.log(e)
    })
    setMaxPrice(max_price);
    setPrice(max_price)
  },[data])
  
  const [category, setCategory] = useState("All");

  const handleCategory = (event) => {
    console.log("Label 👉️", event.target.selectedOptions[0].label);
    console.log(event.target.value);
    setCategory(event.target.value);
  };

  //To be filled after data loads from Rest API
  const [value, setValue] = useState([20, 37]);
  //Fixed range difference > some value(10 in this case)
  const minDistance = 10;

  const handlePrice = (event, newValue) => {
    if (newValue[1] - newValue[0] < minDistance) {
      if (value[0] == newValue[0]) {
        setValue([newValue[0], newValue[0] + minDistance]);
      }
      if (value[1] == newValue[1]) {
        setValue([newValue[1] - minDistance, newValue[1]]);
      }
    } else {
      setValue(newValue);
    }
  };

  // Will be filled after data loads from Rest API
  
  const [location, setLocation] = useState("All");

  const handleLocation = (event) => {
    console.log("Label ", event.target.selectedOptions[0].label);
    console.log(event.target.value);
    setLocation(event.target.value);
  };

  // Will be filled after data loads from Rest API
  const FRESH = ["All", "1 month", "2 week", "3 days"];
  const [freshness, setFreshness] = useState("");

  const handleFreshness = (event) => {
    console.log("Label ", event.target.selectedOptions[0].label);
    console.log(event.target.value);
    setFreshness(event.target.value);
  };

  const updateFilterValue =(event) =>{
    setPrice(event.target.value);
  }

  const submitFilter = () => {
    var tempData = [];

    
    tempData = data.data.filter((e)=>e.price<=price)
    if(location!=="All"){
    tempData = tempData.filter((e)=>e.location===location)
  }
  if(category!=="All"){
    tempData = tempData.filter((e)=>e.category===category)
  }
    console.log(tempData)
    console.log(data)
    console.log(data.setVisData)
    data.setVisData(tempData)
  }

  return (
    <div className="filterCard">
      <div className="Box">
        <div className="dropDowns">
          <div className="FILTER">Filter</div>
          <div className="category">
            <label>Category: </label>
            <select
              className="dropDown"
              value={category}
              onChange={handleCategory}
            >
              {CAT.map((v) => (
                <option value={v} key={v}>
                  {v}
                </option>
              ))}
            </select>
          </div>
          <div className="location">
            <label>Location: </label>
            <select
              className="dropDown"
              value={location}
              onChange={handleLocation}
            >
              {LOC.map((v) => (
                <option value={v} key={v}>
                  {v}
                </option>
              ))}
            </select>
          </div>
          <div className="priceDiv">
            <label>Price: </label>
            <p>{price}</p>
            <input type="range" min={0} max={maxPrice} step={10} onChange={updateFilterValue}/>
          </div>
          <div className="applyFilterButtonClass">
            <button className="applyFilterButton" onClick={submitFilter}>Apply</button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Filter;
