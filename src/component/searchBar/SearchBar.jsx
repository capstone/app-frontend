import { useState, useEffect } from "react";
import React from "react";
import "./searchBar.css";

function SearchBar({ expand }) {
  const [query, setQuery] = useState("");

  useEffect(() => {
    // console.log("query changed", query);
  }, [query]);

  return (
    <>
    <div className="SearchBar"
      style={{height: expand? "30px": "0px", padding: expand? "10px": "0px"}}
    >
      <input type="submit" value="go" className="submit" />
      <input
        className="bar"
        placeholder="Search for..."
        type="text"
        onChange={(e) => setQuery(e.target.value)}
      />
    </div>
    </>
  );
}

export default SearchBar;
