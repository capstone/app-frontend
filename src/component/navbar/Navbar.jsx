import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';
import './navbar.css'

const Navbar = () => {
  return (
    <div className='navBar'>
      <ul className="linksContainer">
        <li className="linkItem"><NavLink exact to="/homepage" className='link' activeClassName="active">Home</NavLink></li>
        <li className="linkItem"><NavLink to="/all-products" className='link' activeClassName="active">Products</NavLink></li>
        <li className="linkItem"><NavLink to="/about" className='link' activeClassName="active">About</NavLink></li>
        <li className="linkItem"><NavLink to="/contact" className='link' activeClassName="active">Contact</NavLink></li>
        <li className="linkItem"><NavLink to="/seller" className='link' activeClassName="active">Seller</NavLink></li>
      </ul>
    </div>
  )
}

export default Navbar
