import React from 'react'
import './footer.css'
import logo_light from './logo1.png'

const Footer = () => {
  return (
    <div className='footer'>
        <div className='footerContent'>
            <img src={logo_light} class="footer_logo" alt="" />
            <div className='footerULContainer'>
                <ul class="footerCategory">
                    <li class="categoryTitle">Help</li>
                    <li><a href="#" class="footerLink">Customer Service</a></li>
                    <li><a href="#" class="footerLink">FAQs</a></li>
                    <li><a href="#" class="footerLink">My Orders</a></li>
                    <li><a href="#" class="footerLink">Contact Us</a></li>
                    <li><a href="#" class="footerLink">Product Recalls</a></li>
                    <li><a href="#" class="footerLink">Return Policy</a></li>
                    <li><a href="#" class="footerLink">Buying Guide</a></li>
                    <li><a href="#" class="footerLink">Selling Guide</a></li>
                    <li><a href="#" class="footerLink">Services</a></li>
                    <li><a href="#" class="footerLink">Feedback</a></li>
                </ul>
                <ul class="footerCategory">
                    <li class="categoryTitle">Legal</li>
                    <li><a href="#" class="footerLink">Privacy and Security</a></li>
                    <li><a href="#" class="footerLink">Privacy Policy</a></li>
                    <li><a href="#" class="footerLink">Terms and Conditions</a></li>
                    <li><a href="#" class="footerLink">Product Registration</a></li>
                    <li><a href="#" class="footerLink">Buyer Registration</a></li>
                    <li><a href="#" class="footerLink">Seller Registration</a></li>
                    <li><a href="#" class="footerLink">Newsroom</a></li>
                    <li><a href="#" class="footerLink">Bidding Policy</a></li>
                    <li><a href="#" class="footerLink">Seller Policy</a></li>
                    <li><a href="#" class="footerLink">About Us</a></li>
                </ul>
            </div>
        </div>
        <div class="footerSocialContainer">
            <div>
                <a href="#" class="socialLink">Terms & Services</a>
                <a href="#" class="socialLink">Privacy Page</a>
            </div>
            <div>
                <a href="#" class="socialLink">Facebook</a>
                <a href="#" class="socialLink">Instagram</a>
                <a href="#" class="socialLink">Twitter</a>
            </div>
        </div>
        <p class="footerCredit">On The Block, 2023</p>
    </div>
  )
}

export default Footer
