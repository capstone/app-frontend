import React from 'react'
import SideBar from '../sideBar/SideBar'
import './header.css'
import header_logo from "./logo2.png";
import header_cart from "./cart.png";
import header_user from "./user.png";
import { useState, useEffect } from 'react';
import Navbar from '../navbar/Navbar';

const Header = ({data, setVisData, allProd}) => {

    // if(allProd == null){
    //   allProd = false;
    // }
    // // console.log(data);
    // if(allProd==true){
    //   setVisData(data);
    // }
    // console.log(data);
    // setVisData(data);
    const [user, setUser] = useState(JSON.parse(localStorage.getItem('userName')));
    const [userPopupVisible, setUserPopupVisible] = useState(false);
    const [searchValue, setSearchValue] = useState('');

    const handleUserImageClick = () => {
        setUserPopupVisible(!userPopupVisible);
      };
    
      const handleActionBtnClick = () => {
        if (user) {
          localStorage.clear();
          window.location.reload();
        } else {
          window.location.href = '/login';
        }
      };

      const handleSearchBtnClick = async () => {
        // event.preventDefault();
        const results = [];
        // console.log(searchValue);
        
        data.map((product)=>{
          if(product?.category.toLowerCase().includes(searchValue.inputVal.toLowerCase()) || product?.condition.toLowerCase().includes(searchValue.inputVal.toLowerCase()) || product?.name.toLowerCase().includes(searchValue.inputVal.toLowerCase()) || product?.location.toLowerCase().includes(searchValue.inputVal.toLowerCase())){
            results.push(product);
          }
        })
        setVisData(results);
      };

  return (
    <div className=''>
      <div className='header'>
        <img src={header_logo} alt="" className="header-logo" />
        <div class="navItems">
            <div class="search">
                <input class="searchBox" type="text" placeholder="Search Items..." value={searchValue?.inputVal} onChange={(e) => {setSearchValue({inputVal: e.target.value})}} />
                <button class="searchBtn" onClick={handleSearchBtnClick}> Search </button>
            </div>
            <a>
                <img src={header_user} id="userImage" alt="" onClick={handleUserImageClick} />
                <div className={`login-logout-popup ${userPopupVisible ? '' : 'hide'}`}>
                <p className="accountInfo">Logged in as, {user ? user : 'Guest'}</p>
                <button className="btn" id="userBtn" onClick={handleActionBtnClick}>
                  {user ? 'Log out' : 'Log in'}
                </button>
                </div>
            </a>
            <a href="/cart"><img src={header_cart} /></a>
        </div>
      </div>
        <Navbar />
    </div>
  )
}

export default Header