// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getAuth } from "firebase/auth";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyD-Df2eUPqe8vXa0-ibNv9yxZqu5qIQWdw",
  authDomain: "capstone2023-6cba5.firebaseapp.com",
  projectId: "capstone2023-6cba5",
  storageBucket: "capstone2023-6cba5.appspot.com",
  messagingSenderId: "58304541230",
  appId: "1:58304541230:web:c3e0434dbee8d1ae6225bf",
  measurementId: "G-3X4LT8SHYQ",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);
export const auth = getAuth();
