import Login from "./pages/login/Login";
import Register from "./pages/register/Register";
import Seller from "./pages/seller/Seller";
import Landing from "./pages/landing/Landing";
import { BrowserRouter as Router, Route, Link, Routes } from "react-router-dom";
import useToken from "./hooks/useToken";
import Product from "./pages/product/Product";
import AddProduct from "./pages/addProduct/AddProduct";
import Homepage from "./pages/homepage/Homepage";
import Cart from "./pages/cart/Cart";
import Checkout from "./pages/checkout/Checkout";
import AllProducts from "./pages/allProducts/AllProducts";
import About from "./pages/about/About";
import Contact from "./pages/contact/Contact";
import UserProfile from "./pages/userProfile/UserProfile";
import EditProduct from "./pages/editProduct/EditProduct";
import SellerDashb from "./pages/sellerDashb/SellerDashb";
import Verification from "./pages/verification/Verification";
import ForgotPassword from "./pages/forgotPassword/ForgotPassword";
import ResetPassword from "./pages/resetPassword/ResetPassword";
import Health from "./Health";

function App() {
  const { token, setToken, userName, setUserName } = useToken();
  return (
    <Router>
      <Routes>
        <Route
          path="/login"
          element={
            <Login
              token={token}
              setToken={setToken}
              setUserName={setUserName}
            />
          }
        />
        <Route
          path="/"
          element={<Homepage token={token} userName={userName} />}
        />
        <Route path="/register" element={<Register />} />
        <Route path="/seller" element={<Seller />} />
        <Route path="/add-product" element={<AddProduct />} />
        <Route path="/homepage" element={<Homepage />} />
        <Route path="/cart" element={<Cart />} />
        <Route path="/checkout" element={<Checkout />} />
        <Route path="/all-products" element={<AllProducts />} />
        <Route path="/about" element={<About />} />
        <Route path="/contact" element={<Contact />} />
        <Route path="/product/:productId" element={<Product />} />
        <Route path="/profile" element={<UserProfile />} />
        <Route path="/editproduct/:productId" element={<EditProduct />} />
        <Route path="/seller-dashboard" element={<SellerDashb />} />
        <Route path="/verification" element={<Verification />} />
        <Route path="/forgot-password" element={<ForgotPassword />} />
        <Route path="/reset-password" element={<ResetPassword />} />
        <Route path="/health" element={<Health />} />
      </Routes>
    </Router>
  );
}

export default App;
