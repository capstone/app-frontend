import "./login.css";
import login_logo from "./logo1.png";
import GoogleLogin from "../../component/auth/GoogleLogin";
import { useAuthState } from "react-firebase-hooks/auth";
import { useNavigate } from "react-router-dom";
import { auth } from "../../utils/firbase";
import { Navigate, Link } from "react-router-dom";
import { useState } from "react";
import axios from "../../api/axios";

const LOGIN_URL = "/login";

const Login = ({ token, setToken, setUserName }) => {

  const [focused1, setFocused1] = useState(false);
  const [focused3, setFocused3] = useState(false);
  const [wrongpwd, setWrongpwd] = useState(false);
  const [lockedUser, setLockedUser] = useState(false);
  const handleFocus1 = (e) => {
    setFocused1(true);
  };
  const handleFocus3 = (e) => {
    setFocused3(true);
  };

  const [user, setUser] = useState("");
  const [pwd, setPwd] = useState("");
  const [enable, setEnable] = useState(true);
  const [googleUser, loading] = useAuthState(auth);
  const navigate = useNavigate();
  const handleSubmit = async (e) => {
    setEnable(false);
    e.preventDefault();
    try {
      const response = await axios.post(
        LOGIN_URL,
        {
          username: user,
          password: pwd,
        },
        {
          header: { "Content-Type": "application/json" },
          withCredentials: false,
        }
      );
      // if (response.status == 500) {
      //   if (response.data.message == 'Your profile has been reported more than 5 times. Please contact the admin at megaminds.capstone@gmail.com . Thank you!') {
      //     setLockedUser(true);
      //   }
      // }
      if (response.status == 200) {
        if (response.data.message == "User has not verified his/her profile") {
          const data = {
            phone: response.data.phone,
            email: response.data.email,
          };
          navigate("/verification", {state: data});
        }
        else{
          setToken(response.data.access_token);
          setUserName(user);
          navigate("/homepage");
        }
        
      }
    } catch (error) {
      if (error.response && error.response.status === 500) {
        const errorMessage = error.response.data.message;
    
        if (errorMessage === 'Your profile has been reported more than 5 times. Please contact the admin at megaminds.capstone@gmail.com . Thank you!') {
          setLockedUser(true);
        }
      } else if (!error.message) {
        console.log('No response from the server');
      } else {
        console.log(error.message);
        setWrongpwd(true);
      }
    }
    setEnable(true);
  };

  return (
    <>
      {token && <Navigate to="/homepage" replace={true} />}
      <div className="login">
        <div className="card">
          <div className="left">
            <img src={login_logo} alt="" className="login_logo" />
            <p className="logo-tagline">
              One-stop for college students. Everything your home deserves at
              affordable prices!
            </p>
            <span className="logo-text">Don't have an account?</span>
            <button
              className="register-button"
              onClick={() => navigate("/register")}
            >
              Register
            </button>
          </div>
          <div className="right">
            <h1 className="login-header">Login</h1>
            {!wrongpwd && lockedUser && <div className="error">Your profile has been reported more than 5 times. Please contact the admin at megaminds.capstone@gmail.com . Thank you!</div>}
            {!lockedUser && wrongpwd && <div className="error">Wrong Password or UserId.</div>}
            <form onSubmit={handleSubmit}>
              <input
                type="text"
                placeholder="Username"
                required
                onBlur={handleFocus1}
                focused1={focused1.toString()}
                pattern="^[A-Za-z0-9]{3,30}$"
                onChange={(e) => setUser(e.target.value)}
              />
              <span id="nameError"><p>Please enter a valid username between 3 to 30 characters without any special characters.</p></span>
              
              <input
                type="password"
                placeholder="Password"
                required
                onBlur={handleFocus3}
                focused3={focused3.toString()}
                pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*])[A-Za-z\d!@#$%^&*]{8,}$"
                onChange={(e) => setPwd(e.target.value)}
              />
              <span id="passwordError"><p>Please enter a valid password. It should be atleast 8 characters long with 1 Uppercase letter, 1 digit and 1 special character.</p></span>
              <div className="buttons" id="login-id">
                {enable && <button className="login-button">Login</button>}
              </div>
              <Link className="forgot-button" id="forgot-id" to = {"/forgot-password"}>Forgot Password</Link>
            </form>
            <GoogleLogin />
          </div>
        </div>
      </div>
    </>
  );
};

export default Login;
