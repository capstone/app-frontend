import React, { useRef } from 'react';
import { useState } from 'react'
import './feedback.css'
import closePic from './close.png'
import emailjs from '@emailjs/browser';
import axios from '../../api/axios';
import ClipLoader from "react-spinners/ClipLoader";

const Feedback = ({onClose, visible, data}) => {
    const form = useRef();
    console.log(data)
    const [reported, setReported] = useState(false);
    const [error, setError] = useState(false);
    const [loading, setLoading] = useState(false);
    const Result = () => {
        return (
            <p>Your message has been sent. We will contact you soon</p>
        );
    };
    const report_URL = "/report-user";

    const [result, showResult] = useState(false);
    const sendEmail = (e) => {
        e.preventDefault();
    
        emailjs.sendForm('service_hh53z9e', 'template_rujytgl', form.current, 'aGKztHbxhFMHvw-yO')
          .then((result) => {
              console.log(result.text);
          }, (error) => {
              console.log(error.text);
          });
          e.target.reset();
          showResult(true);
      };

    const handleOnClose = (e) => {
    if(e.target.id ==='feedbackContainer')
        onClose()
        showResult(false);
  }


  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    const token = localStorage.getItem('token');
    const tokenWithoutQuotes = token.replace(/"/g, '');
    console.log(tokenWithoutQuotes)
    console.log("submit")
    console.log(data.seller_id)
    e.preventDefault();
    try {
      const response = await axios.post(
        report_URL,
        {
            reported_user: data.seller_id
        },
        {
          headers: { 'Authorization': `Bearer ${tokenWithoutQuotes}`, 
                    "Content-Type": "application/json" 
                },
        }
      )
      setLoading(false);
      console.log(response)
      if (response.status == 200) {
        console.log(response)
        setReported(true)
        setError(false)
        setLoading(false);
      }
    } catch (err) {
        setError(true);
        setLoading(false);
      if (!err.message) {
        console.log("No response from the server");
      } else {
        console.log(err.message);
      }
    }
  }

  if (!visible) return null

  return (
    <div className='feedbackPage' id='feedbackContainer' onClick={handleOnClose}>
        <div className='feedbackPopUp'>
            <img src={closePic} alt="" className="closePic" onClick={onClose} />
            {reported && <h3 className="reportHeading">
                You have successfully reported seller 
            </h3>}
            {loading && <div className='spinnerDiv'>
            <ClipLoader
                color={"yellow"}
                loading={true}
                size={40}
                aria-label="Loading Spinner"
                data-testid="loader"
            />
            </div>}
            {error && 
            <>
            <h3 className="reportHeading">
            Some error occurred
        </h3>
            </>
            }
            <p className="reportSubHeading">We appreciate your contribution in making our platform secure.</p>
            {!loading && !reported && <button className="btn" id='applyBtn' onClick={handleSubmit}>
                Report
            </button>}
            
            {/* <form className="inputBox" ref={form} onSubmit={sendEmail}>
                <input type="text" className="reporterName" name="reporterName" placeholder="Full Name"/>
                <input type="text" className="reporterEmail" name="reporterEmail" placeholder="Your Email" />
                <input type="text" className="reportedSellerID" name='reportedSellerID' placeholder="Seller ID" />
                <input type="text" className="reportedSellerName" name='reportedSellerName' placeholder="Seller Name" />
                <input type="text" className="reportedProduct" name='reportedProduct' placeholder="Product Name" />
                <input type="text" className="reportReason" name='reportReason' placeholder="Reason" />
                <div className="reportBtn">
                    <button className="btn" id='applyBtn' onClick={handleSubmit}>
                        Submit
                    </button>
                    <div className='resultMessage'>{result? <Result/> : null}</div>
                </div>
            </form> */}
        </div>
    </div>
  )
}

export default Feedback
