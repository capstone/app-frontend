import React from 'react'
import Footer from '../../component/footer/Footer'
import Header from '../../component/header/Header'
import './cart.css'
import empty_cart from './empty-cart.png'

const Cart = () => {
  return (
    <div className='cartPage'>
        <Header />
        <div className="cartSection">
            <div className='productList'>
                <p className='sectionHeading'>Your Cart</p>
                <div className='cartArea'>
                    <img src={empty_cart} class="emptyImg" alt="" />
                </div>
                <p className='sectionHeading'>Your Wishlist</p>
                <div className='wishlistArea'>
                    <img src={empty_cart} class="emptyImg" alt="" />
                </div>
            </div>
            <div class="checkoutSection">
                <div class="checkoutBox">
                    <p class="text">your total bill</p>
                    <h1 class="bill">$00</h1>
                    <a href="/checkout" class="checkoutBtn">checkout</a>
                </div>
            </div>
        </div>
        <Footer />
    </div>
  )
}

export default Cart
