import "./resertPassword.css";
import login_logo from "./logo1.png";
import GoogleLogin from "../../component/auth/GoogleLogin";
import { useAuthState } from "react-firebase-hooks/auth";
import { useNavigate } from "react-router-dom";
import { auth } from "../../utils/firbase";
import { Navigate } from "react-router-dom";
import { useState } from "react";
import axios from "../../api/axios";

const RESET_URL = "/reset-password";

const ResetPassword = ({}) => {
  const queryParameters = new URLSearchParams(window.location.search)
  const token = queryParameters.get("token")
  const username = queryParameters.get("username")
  const navigate = useNavigate();
  const [pwd, setPwd] = useState("");
  const [focus, setFocus] = useState(false);
  const [focus1, setFocus1] = useState(false);
  const [enable, setEnable] = useState(false);
  const [wrongEmail, setWrongEmail] = useState(false);
  const [success, setSuccess] = useState(false);
  const [match, setMatch] = useState(false);

  
  const handleSubmit = async (e) => {
    setEnable(false);
    e.preventDefault();
    let formData = new FormData();
    formData.append("password", pwd)
    formData.append("confirm_password", pwd)
    formData.append("token", token)
    formData.append("username", username)
try {
      const response = await axios.post(
        RESET_URL,
        formData,
        {
          header: { 
            "Authorization": `Bearer ${token}`,
            "Content-Type": "form/data"
        }
        }
      );
      if (response.status == 200) {
        setSuccess(true);
        setWrongEmail(false);
      }
    } catch (err) {
      if (!err.message) {
        console.log("No response from the server");
      } else {
        console.log(err.message);
        setWrongEmail(true);
        setSuccess(false);
      }
    }
    setEnable(true);
  };

  const handleFocus = (e) => {
    setFocus(true);
  };
  const handleFocus1 = (e) => {
    setFocus1(true);
  };
  const handlePasswordMatch = (e) => {
    var format = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*])[A-Za-z\d!@#$%^&*]{8,}$/;
    console.log(pwd);
    console.log(e.target.value);
    console.log(format.test(pwd))
   
    if(pwd.match(format) && e.target.value===pwd ){
        setMatch(true);
    }
    else{
        setMatch(false);
    }
    console.log(match)
  };

  return (
    <>
    {console.log(token)}
      <div className="login">
        <div className="card">
          <div className="left">
            <img src={login_logo} alt="" className="login_logo" />
            <p className="logo-tagline">
              One-stop for college students. Everything your home deserves at
              affordable prices!
            </p>
            <span className="logo-text">Don't have an account?</span>
            <button
              className="register-button"
              onClick={() => navigate("/register")}
            >
              Register
            </button>
          </div>
        </div>
        <div className="right">
          {wrongEmail && <div className="error">Error: User not found</div>}
          {success && <div className="success">Password has been successfully reset.</div>}
          {success && <button className="login-button" onClick={navigate("/login")} >
            Login
          </button>}
          <input
            type="text"
            placeholder="New Password"
            required
            onChange={(e) => setPwd(e.target.value)}
            pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*])[A-Za-z\d!@#$%^&*]{8,}$"
            focus={focus.toString()}
            onBlur={handleFocus}
          />
          <span id="emailError">
            <p>Please enter a valid password. It should be atleast 8 characters long with 1 Uppercase letter, 1 digit and 1 special character.</p>
          </span>
          <input
            type="text"
            placeholder="Re-enter Password"
            required
            onChange={(e) => handlePasswordMatch(e)}
          />
          {match===false &&
            <p className="error">Password doesn't match</p>
            }
          {match && 
          <button className="login-button" onClickCapture={handleSubmit}>
            Submit
          </button>}
        </div>
      </div>
    </>
  );
};

export default ResetPassword;
