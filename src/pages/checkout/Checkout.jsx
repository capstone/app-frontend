import React from 'react'
import Footer from '../../component/footer/Footer'
import Header from '../../component/header/Header'
import './checkout.css'
import empty_cart from './empty-cart.png'

const Checkout = () => {
  return (
    <div className=''>
        <Header />
        <div className='checkoutPage'>
            <div className="chekoutForm">
                <h1 class="checkoutHeading">Checkout</h1>
                <p class="addressText">Delivery Address</p>
                <input type="text" id="address" placeholder="Address" />
                <input type="text" id="street" placeholder="Street" />
                <div class="threeInputContainer">
                    <input type="text" id="city" placeholder="City" />
                    <input type="text" id="state" placeholder="State" />
                    <input type="text" id="pincode" placeholder="Pincode" />
                </div>
                <input type="text" id="landmark" placeholder="Landmark" />
            </div>
            <div class="cartSection">
                <div class="productList">
                    <p class="sectionHeading">Your Cart</p>
                    <div class="cartArea">
                        <img src={empty_cart} class="emptyImg" alt="" />
                    </div>
                </div>
                <div class="checkoutSection">
                    <div class="checkoutBox">
                        <p class="text">your total bill,</p>
                        <h1 class="bill">$00</h1>
                        <button class="placeOrderButton">Place Order</button>
                    </div>
                </div>
            </div>
        </div>
        <Footer />
    </div>
  )
}

export default Checkout
