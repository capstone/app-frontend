import React from 'react'
import './editProfile.css'
import closePic from './close.png'
import axios from "../../api/axios";

const EditProfile = ({onClose, visible}) => {

    const handleOnClose = (e) => {
        if(e.target.id ==='editProfileContainer')
            onClose()
      }

    if (!visible) return null

  return (
    <div className='editProfilePage' id='editProfileContainer' onClick={handleOnClose}>
      <div className='editProfilePopUp'>
            <img src={closePic} alt="" className="closePic" onClick={onClose} />
            <h3 className="reportHeading">
                Edit Your Information
            </h3>
            {/* <div className='divideSellerContact'>
            <div className='sellerContactInfo'>
                <p>Username: {seller.username}</p>
                <p>Email: {seller.email}</p>
                <p>College: {seller.college}</p>
                <p>Seller Rating: {seller.avg_seller_rating ? seller.avg_seller_rating : 0 }</p>
            </div>
            <div className='sellerContactEmail'>
                <form className="inputBox" >
                    <input type="text" className="buyerName" name="buyerName" placeholder="Full Name"/>
                    <input type="text" className="buyerEmail" name="buyerEmail" placeholder="Your Email" />
                    <textarea className="buyerMessage" name='buyerMessage' id="des" placeholder="Message to Seller"></textarea>
                    <div className="reportBtn">
                        <button className="btn" id='applyBtn'>
                            Send Email
                        </button>
                        <div className='resultMessage'></div>
                    </div>
                </form>
            </div>
            </div> */}
        </div>
    </div>
  )
}

export default EditProfile
