import React, { useRef } from 'react';
import { useState } from 'react'
import Footer from '../../component/footer/Footer'
import Header from '../../component/header/Header'
import './contact.css'
import emailjs from '@emailjs/browser';

const Contact = () => {

  const form = useRef();

  const Result = () => {
    return (
        <p>Your message has been sent. Thank you!</p>
    );
};

const [result, showResult] = useState(false);

const [user, setUser] = useState("");
const [phone, setPhone] = useState("");
const [email, setEmail] = useState("");
const [message, setMessage] = useState("");
const [errorMessage, setErrorMessage] = useState("");

const [focused1, setFocused1] = useState(false);
const [focused2, setFocused2] = useState(false);
const [focused3, setFocused3] = useState(false);
const [focused4, setFocused4] = useState(false);

const handleFocus1 = (e) => {
    setFocused1(true);
  };
  const handleFocus2 = (e) => {
    setFocused2(true);
  };
  const handleFocus3 = (e) => {
    setFocused3(true);
  };
  const handleFocus4 = (e) => {
    setFocused4(true);
  };

const sendEmail = (e) => {
    e.preventDefault();

    emailjs.sendForm('service_45s925f', 'template_76gn4u1', form.current, 'cWvGXMwQlmoQ5fAtW')
      .then((result) => {
          console.log(result.text);
      }, (error) => {
          console.log(error.text);
      });
      e.target.reset();
      showResult(true);
      setErrorMessage("");
      setFocused1(false);
      setFocused2(false);
      setFocused3(false);
      setFocused4(false);
  };

  return (
    <div className='contactPage'>
        <Header />

        <div className='contact'>
            <p class="aboutheading">Contact Us</p>
            <p class="desc">Thank you for choosing our platform for your second-hand goods trading needs. We're committed to providing you with the best possible experience, and we're always here to help.</p>
            <p class="desc">We're always looking for ways to improve our platform and make it even better for our users. If you have any feedback or suggestions, please let us know! We value your input and will do our best to incorporate your ideas into our platform.</p>
            <p class="desc">If you have any questions about using our platform or need help with a transaction, please submit a message below. We'll do our best to respond to your message within 24 hours.</p>
            <p class="desc">If you prefer to speak with someone directly, you can call us at 180-000-0001 during our business hours, 9 AM - 5 PM, Monday-Friday. Our team will be happy to assist you with any questions or concerns you may have.</p>
            <form className="contactForm" ref={form} onSubmit={sendEmail}>
                <input 
                type="text" 
                id="contactfullName" 
                name="user_name" 
                placeholder="Full Name" 
                title="Please enter a valid name between 3 and 30 charaters."
                onChange={(e) => setUser(e.target.value)}
                onBlur={handleFocus1}
                focused1={focused1.toString()}
                pattern="^[A-Za-z0-9]{3,30}$"
                required/>
                <span id="nameError"><p>Please enter a valid username between 3 to 30 characters without any special characters.</p></span>
                
                <input 
                type="text" 
                id="contactemail" 
                name="user_email_id" 
                placeholder="Email" 
                onChange={(e) => setEmail(e.target.value)}
                onBlur={handleFocus2}
                focused2={focused2.toString()}
                pattern="^[A-Za-z0-9._%+-]+@vt\.edu$"
                required />
                <span id="emailError"><p>Please enter a valid VT email id.</p></span>
                
                <input 
                type="text" 
                id="contactnumber" 
                name="user_phone" 
                placeholder="Number" 
                title="Please enter a valid 10-digit phone number"
                onChange={(e) => setPhone(e.target.value)}
                onBlur={handleFocus3}
                focused3={focused3.toString()}
                pattern="^\+1[0-9]{10}$"
                required />
                <span id="phoneError"><p>Please enter a valid Phone Number with the format +1XXXXXXXXXX &#40;+1 with 10 digits after&#41;.</p></span>

                <input 
                type="text" 
                id="contactmessage" 
                name="user_message" 
                placeholder="Message" 
                required 
                minLength="10" 
                maxLength="300" 
                onChange={(e) => setMessage(e.target.value)}
                onBlur={handleFocus4}
                focused4={focused4.toString()}
                title="Please enter a message between 10 and 300 characters" />
                <span id="messageError"><p>Please enter a valid message between 10 and 300 characters.</p></span>

                <div className="reportBtn">
                    <button className="btn" id='applyBtn'>
                        Submit
                    </button>
                    <div className='resultMessage'>{result? <Result/> : null}</div>
                </div>
            </form>
        </div>

        <Footer />
    </div>
  )
}

export default Contact
