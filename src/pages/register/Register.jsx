import "./register.css";
import reg_logo from "./logo1.png";
import axios from "../../api/axios";
import { userRef, useState } from "react";
import { useNavigate } from "react-router-dom";

//End point in backend API
const REGISTER_URL = "/register";

const Register = () => {
  
  const [user, setUser] = useState("");
  const [phone, setPhone] = useState("");
  const [pwd, setPwd] = useState("");
  const [email, setEmail] = useState("");
  const [college, setCollege] = useState("");
  const [enable, setEnable] = useState(true);
  const [errorMessage, setErrorMessage] = useState("");
  
  const navigate = useNavigate();
  const [focused1, setFocused1] = useState(false);
  const [focused2, setFocused2] = useState(false);
  const [focused3, setFocused3] = useState(false);
  const [focused4, setFocused4] = useState(false);
  const [focused5, setFocused5] = useState(false);

  const handleFocus1 = (e) => {
    setFocused1(true);
  };
  const handleFocus2 = (e) => {
    setFocused2(true);
  };
  const handleFocus3 = (e) => {
    setFocused3(true);
  };
  const handleFocus4 = (e) => {
    setFocused4(true);
  };
  const handleFocus5 = (e) => {
    setFocused5(true);
  };
  
  const handleSubmit = async (e) => {
    setEnable(false);
    e.preventDefault();
    try {
      console.log("Sending: ");
      console.log(
        JSON.stringify({
          username: user,
          password: pwd,
          email: email,
          phone: phone,
          college: college,
        })
      );
      const response = await axios.post(
        REGISTER_URL,
        {
          username: user,
          password: pwd,
          email: email,
          phone: phone,
          college: college,
        },
        {
          header: { "Content-Type": "application/json" },
          withCredentials: false,
        }
      );
      setEnable(true);
      const data = {
        phone: phone,
        email: email,
      };
      navigate("/verification", {state: data });
    } catch (err) {
      if (!err.message) {
        console.log("No response from the server");
        setErrorMessage("No response from the server");
      } else {
        console.log(err.message);
        setErrorMessage("There was an issue with registration: " + err.response.data.message);
      }
      setEnable(true);
    }
    
  };

  return (
    <div className="register">
      <div className="register-card">
        <div className="left">
          <img src={reg_logo} alt="" className="reg-logo" />
          <p className="reg-logo-tagline">
            One-stop for college students. Everything your home deserves at
            affordable prices!
          </p>
          <span className="reg-logo-text">Already have an account?</span>
          <button
            className="register-button"
            onClick={() => navigate("/login")}
          >
            Login
          </button>
        </div>
        <div className="right">
          <h1 className="register-header">Register</h1>
          <form className='registerForm' onSubmit={handleSubmit}>
            <input
              type="text"
              placeholder="Username"
              onChange={(e) => setUser(e.target.value)}
              onBlur={handleFocus1}
              focused1={focused1.toString()}
              pattern="^[A-Za-z0-9]{3,30}$"
              required
            />
            <span id="nameError"><p>Please enter a valid username between 3 to 30 characters without any special characters.</p></span>
            <input
              type="email"
              placeholder="Email"
              onChange={(e) => setEmail(e.target.value)}
              onBlur={handleFocus2}
              focused2={focused2.toString()}
              pattern="^[A-Za-z0-9._%+-]+@vt\.edu$"
              required
            />
            <span id="emailError"><p>Please enter a valid VT email id.</p></span>

            <input
              type="text"
              placeholder="Phone Number"
              onChange={(e) => setPhone(e.target.value)}
              onBlur={handleFocus3}
              focused3={focused3.toString()}
              // pattern="^(\+\d{1,2}\s?)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$"
              pattern="^\+1[0-9]{10}$"
              required
            />
            <span id="phoneError"><p>Please enter a valid Phone Number with the format +1XXXXXXXXXX &#40;+1 with 10 digits after&#41;.</p></span>

            <input
              type="password"
              placeholder="Password"
              onChange={(e) => setPwd(e.target.value)}
              onBlur={handleFocus4}
              focused4={focused4.toString()}
              pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*])[A-Za-z\d!@#$%^&*]{8,}$"
              required
            />
            <span id="passwordError"><p>Please enter a valid password. It should be atleast 8 characters long with 1 Uppercase letter, 1 digit and 1 special character.</p></span>

            <input
              type="text"
              placeholder="College Name"
              onChange={(e) => setCollege(e.target.value)}
              onBlur={handleFocus5}
              focused5={focused5.toString()}
              pattern="^(VT|Virginia Tech|VTech)$"
              required
            />
            <span id="collegeError"><p>Please enter a valid college name.</p></span>

            {errorMessage && <p style={{color: 'red'}}>{errorMessage}</p>}
            
            {enable && <button className="login-button">Register</button>}
          </form>
        </div>
      </div>
    </div>
  );
};

export default Register;
