import React from 'react'
import { useState, useEffect } from 'react';
import './sellerContact.css'
import closePic from './close.png'
import axios from "../../api/axios";
import {Rating} from 'react-simple-star-rating';
import ClipLoader from "react-spinners/ClipLoader";


const SellerContact = ({onClose, visible, productId}) => {
    const [seller, setSeller] = useState(null);

    const[buyerName, setBuyerName] = useState("");
    const[buyerEmail,setBuyerEmail] = useState("");
    const[buyerMessage,setBuyerMessage] = useState("");
    const[sentMess, setSentMess] = useState(false);
    const [loading, setLoading] = useState(false);
    const [validEmail, setValidEmail] = useState(false);

    const handleSubmit = async(e)=>{
      setLoading(true)
        e.preventDefault();
        const token = localStorage.getItem('token');
        const tokenWithoutQuotes = token.replace(/"/g, '');
        const formData = new FormData();
        formData.append("buyer_name", buyerName);
        formData.append("buyer_email", buyerEmail);
        formData.append("message", buyerMessage);
        formData.append("seller_email", seller.email)
        axios.post(
          "/send-email", formData, 
          {
            headers: {
              'Authorization': `Bearer ${tokenWithoutQuotes}`,
              'Content-Type': 'multipart/form-data'
            }
          }
        ).then(
          (response)=>{
            setLoading(false);
            if(response.status===200){
              console.log("success")
              setSentMess(true)
            }
            else{
              console.log("fail")
              console.log(response)
            }
          }
        )
    }

    useEffect(() => {
        const response = axios.get(`/product/${productId}/seller-contact`)
          .then(response => {
            setSeller(response.data.seller_contact);
          })
          .catch(error => {
            console.log(error);
          });
          console.log(response)
      }, [productId]);

    const handleOnClose = (e) => {
        if(e.target.id ==='sellerContactContainer')
            onClose()
      }
      
      if (!seller) {
        return <div>Loading seller contact information...</div>;
      }

      if (!visible) return null

  const emailCheck = async (e) => {
    setBuyerEmail(e.target.value)
    console.log(buyerEmail)
    var pattern="^[A-Za-z0-9._%+-]+@vt\.edu$"
    if(e.target.value.match(pattern)){
      setValidEmail(true);
  }
  else{
    setValidEmail(false);
  }
  console.log(validEmail)
  }
    
  return (
    <div className='sellerContactPage' id='sellerContactContainer' onClick={handleOnClose}>
        <div className='sellerContactPopUp'>
            <img src={closePic} alt="" className="closePic" onClick={onClose} />
            <h3 className="reportHeading">
                Seller Contact Information
            </h3>
            <p className="reportSubHeading">You can send an email to seller for further information.</p>
            <div className='divideSellerContact'>
            <div className='sellerContactInfo'>
                <p>Username: {seller.username}</p>
                <p>Email: {seller.email}</p>
                <p>College: {seller.college}</p>
                {/* <p>Seller Rating: {seller.avg_seller_rating ? seller.avg_seller_rating : 0 }</p> */}
                <p>Seller Rating: <Rating ratingValue={seller.avg_seller_rating ? Math.round(seller.avg_seller_rating * 100) / 100 : 0 } size={20} readonly = {true} /></p>
            </div>
            <div className='sellerContactEmail'>
              {loading && <div className='spinnerDiv'>
                <ClipLoader
                    color={"yellow"}
                    loading={true}
                    size={40}
                    aria-label="Loading Spinner"
                    data-testid="loader"
                />
                </div>
              }
                {!loading && !sentMess && <form className="inputBox" >
                    <input type="text" className="buyerName" name="buyerName" placeholder="Full Name" onChange={(e)=>{setBuyerName(e.target.value)}}/>
                    <input type="text" className="buyerEmail" name="buyerEmail" placeholder="Your Email" onChange={(e)=>{emailCheck(e)}}/>
                    {!validEmail && <div className="error">Enter valid Virginia Tech email!</div>}
                    <textarea className="buyerMessage" name='buyerMessage' id="des" placeholder="Message to Seller" onChange={(e)=>{setBuyerMessage(e.target.value)}}/>
                    <div className="reportBtn">
                        {validEmail && <button className="btn" id='applyBtn' onClick={handleSubmit}>
                            Send Email
                        </button>}
                        <div className='resultMessage'></div>
                    </div>
                </form>}
                {
                  sentMess &&
                  <h3 className="reportHeading">
                    Your message has been delivered!
                </h3>
                }
            </div>
            </div>
        </div>
    </div>
  )
}

export default SellerContact
