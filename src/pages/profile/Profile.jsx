import "./profile.css";
import Navbar from "../../component/navbar/Navbar";
import { Navigate } from "react-router-dom";


function Profile(token, userName) {
  return (
    <>
      {!token && <Navigate to="/login" replace={true} />}
      <div className="landing" id="MainPage">
        <Navbar />
      </div>
    </>
  )
}

export default Profile