import React from 'react'
import add_prod_logo from './logo2.png'
import "./addProduct.css";
import { useState, useEffect } from 'react';
import axios from '../../api/axios';

const AddProduct = () => {
      const [name, setName] = useState('');
      const [description, setDescription] = useState('');
      const [price, setPrice] = useState('');
      const [category, setCategory] = useState('');
      const [condition, setCondition] = useState('');
      const [location, setLocation] = useState('');
      const [marketAvailableDate, setMarketAvailableDate] = useState('');
      const [files, setFiles] = useState([]);
      const [productId, setProductId] = useState('');
      const [selectedFiles, setSelectedFiles] = useState([]);
      const [firstSelectedFileUrl, setfirstSelectedFileUrl] = useState('');

      const handleFileUpload = (event) => {
        const newFiles = [...files, ...event.target.files];
        setFiles(newFiles);
      }
      
      useEffect(() => {
        const selectedFilesArray = [];
      
        for (let i = 0; i < files.length; i++) {
          const url = URL.createObjectURL(files[i]);
          selectedFilesArray.push({ file: files[i], url: url });
        }
      
        setSelectedFiles(selectedFilesArray);
        if (selectedFilesArray.length > 0) {
          setfirstSelectedFileUrl(selectedFilesArray[0])
        }
      }, [files]);

      const handleSubmit = (event) => {
        event.preventDefault();
        const token = localStorage.getItem('token');
        const tokenWithoutQuotes = token.replace(/"/g, '');
      
        console.log("Sending: ");
        console.log(
          JSON.stringify({
            name:name,
            description:description,
            price:price,
            category:category,
            condition:condition,
            location:location,
            market_available_date: marketAvailableDate
          })
        );
      
        axios.post('/product_post', {
          name:name,
          description:description,
          price:price,
          category:category,
          condition:condition,
          location:location,
          market_available_date: marketAvailableDate
        },
        {
          headers: {
            "Authorization": `Bearer ${tokenWithoutQuotes}`,
            "Content-Type": "application/json"
          }
        })
        .then((response) => {
          console.log(response)
          setProductId(response.data.product_id);
          console.log("here", productId)
          if(response.data.product_id) {
            console.log(response.data.product_id);
            const formData = new FormData();
            console.log(files)
            console.log(files.length)
            for (let i = 0; i < files.length; i++) {
              formData.append('file', files[i]);
            }
            console.log(response.data.product_id)
            formData.append('product_id', parseInt(response.data.product_id));
            console.log(formData)
              axios.post('/upload_files', formData, {
                headers: {
                  'Authorization': `Bearer ${tokenWithoutQuotes}`,
                  'Content-Type': 'multipart/form-data'
                }
              })
              .then((imageResponse) => {
                console.log(response.data.product_id)
                console.log(imageResponse.data);
              })
              .catch((error) => {
                console.error(error);
              });
            }
            window.history.back();
          })
          .catch((err) => {
            if (err.response) {
              console.log(err.response.data);
            } else if (err.message) {
              console.log(err.message);
            } else {
              console.log("No response from the server");
            }
          });
        };

  return (
    <div className="addNewProduct">
        <img src={add_prod_logo} class="add_new_prod_logo" alt=""></img>
        <div class="form">
            <input type="text" id="productname" maxlength="50" placeholder="Product Name" value={name} onChange={(event) => setName(event.target.value)} required />
            <textarea id="des" placeholder="Detailed Description" value={description} onChange={(event) => setDescription(event.target.value)} required></textarea>
        </div>
        <div class="productInfo">
            <div className="productImage">
              {selectedFiles.length > 0 && firstSelectedFileUrl ? (
                <img className='mainProductImageUpload' src={firstSelectedFileUrl.url} alt={firstSelectedFileUrl.file.name} />
              ) : (
                <p className="textprod">Product Image</p>
              )}
            </div>
            <div class="uploadImageSection">
                <p class="textprod1"><img src="../img/camera.png" alt="" />Upload Image</p>
                <input type="file" name = "file" onChange={handleFileUpload} multiple='true' />
                <div className='uploadImagesPreview'>
                {selectedFiles.map((file) => (
                  <img src={file.url} alt={file.file.name} key={file.file.name} style={{ width: '100px', height: '100px', objectFit: 'cover', margin: '10px' }} />
                ))}
                </div>
            </div>
            <div class="selectSizes">
                <p class="textprod1">Item Condition</p>
                <div class="sizes" >
                    <input type="radio" name="itemCondition" class="sizeCheckbox" id="xs" value="New" checked={condition === "New"} onChange={(event) => setCondition(event.target.value)}/>
                    <input type="radio" name="itemCondition" class="sizeCheckbox" id="s" value="Almost New" checked={condition === "Almost New"} onChange={(event) => setCondition(event.target.value)} />
                    <input type="radio" name="itemCondition" class="sizeCheckbox" id="s" value="Good" checked={condition === "Good"} onChange={(event) => setCondition(event.target.value)}/>
                    <input type="radio" name="itemCondition" class="sizeCheckbox" id="m" value="Normal Wear" checked={condition === "Normal Wear"} onChange={(event) => setCondition(event.target.value)}/>
                    <input type="radio" name="itemCondition" class="sizeCheckbox" id="m" value="Used" checked={condition === "Used"} onChange={(event) => setCondition(event.target.value)}/>
                    <input type="radio" name="itemCondition" class="sizeCheckbox" id="l" value="Highly Used" checked={condition === "Highly Used"} onChange={(event) => setCondition(event.target.value)}/>
                </div>
            </div>
        </div>

        <div class="productPrice">
            <input type="number" id="actualPrice" placeholder="Actual Price (in dollars)" value={price} onChange={(event) => setPrice(event.target.value)} required />
            <input type="text" id="location" placeholder="Location" value={location} onChange={(event) => setLocation(event.target.value)} required/>
            <input type="date" id="dateCreated" placeholder="Available On" value={marketAvailableDate} onChange={(event) => setMarketAvailableDate(event.target.value)} min={new Date().toISOString().split('T')[0]}/>
        </div>

        <textarea id="tags" placeholder="Enter categories here. For example - Couch, Table, Chair" value={category} onChange={(event) => setCategory(event.target.value)}></textarea>
        <input type="checkbox" class="checkbox" id="tac" checked />
        <label for="tac">All the information entered is true as per my knowledge.</label>
        
        <div class="buttons" onClick={handleSubmit}>
            <button class="btn" id="addBtn">Add Product</button>
            <button class="btn" id="saveBtn">Save Draft</button>
        </div>
    </div>
  )
}

export default AddProduct
