import React from 'react'
import './homepage.css'
import home_logo from "./logo1.png";
import cart from "./cart.png";
import userImage from "./user.png";
import video from "./video1.mp4";
import poster1 from "./poster1.png";
import poster3 from "./poster3.png";
import poster4 from "./poster4.png";
import poster5 from "./poster5.png";
import poster6 from "./poster6.png";
import user1 from "./user1.png";
import user2 from "./user2.png";
import user3 from "./user3.png";
import user4 from "./user4.png";
import table1 from "./table1.png";
import sofa1 from "./sofa1.png";
import bed1 from "./bed1.png";
import light1 from "./light1.png";
import SideBar from "../../component/sideBar/SideBar";
import Footer from '../../component/footer/Footer';
import { useState, useEffect } from 'react';
import UserProfile from '../userProfile/UserProfile';

const Homepage = () => {

    const [user, setUser] = useState(JSON.parse(localStorage.getItem('userName')));
    const [userPopupVisible, setUserPopupVisible] = useState(false);
    const [searchValue, setSearchValue] = useState('');

    const handleUserImageClick = () => {
        setUserPopupVisible(!userPopupVisible);
      };
    
      const handleActionBtnClick = () => {
        if (user) {
          localStorage.clear();
          window.location.reload();
        } else {
          window.location.href = '/login';
        }
      };

      const handleUserProfileClick = () => {
        window.location.href = '/profile';
      }

      const handleSellerDashboardClick = () => {
        window.location.href = '/seller';
      }

      const handleSearchBtnClick = () => {
        if (searchValue.length) {
          window.location.href = `search/${searchValue}`;
        }
      };

  return (
    <div>
        <SideBar />
        <section class="showcase">
            <header class="homepageHeader">
                <img src={home_logo} alt="" className="home-logo" />
                <nav class="navbar">
                    <ul class="linksContainer">
                        <li class="linkItem"><a href="/homepage" class="link active">home</a></li>
                        <li class="linkItem"><a href="/all-products" class="link">products</a></li>
                        <li class="linkItem"><a href="/about" class="link">about</a></li>
                        <li class="linkItem"><a href="/contact" class="link">contact</a></li>
                        <li class="linkItem"><a href="/seller" class="link">seller</a></li>
                    </ul>
                    <div class="userInteractions">
                        <div class="user">
                            <img src={userImage} className="userIcon" alt="" onClick={handleUserImageClick}/>
                            <div className={`login-logout-popup ${userPopupVisible ? '' : 'hide'}`}>
                                <p className="accountInfo">Logged in as, {user ? user : 'Guest'}</p>
                                <button className="btn" id="userBtn" onClick={handleActionBtnClick}>
                                    {user ? 'Log out' : 'Log in'}
                                </button>
                                {user && <button className="seeProfileBtn" id="" onClick={handleUserProfileClick}>
                                    See Your Profile
                                </button>}
                                {user && <button className="seeProfileBtn" id="" onClick={handleSellerDashboardClick}>
                                    Seller Dashboard
                                </button>}
                            </div>
                        </div>
                        <div class="cart">
                            <a href='/cart'><img src={cart} className="cartIcon" alt="" /></a>
                            <a href='/cart'><span class="cartItemCount">00</span></a>
                        </div>
                    </div>
                </nav>
            </header>
            <video muted loop autoPlay id='video'>
                <source src={video} type='video/mp4' />
            </video>

            <div class="overlay"></div>
            <div class="text">
                <h2>Simplicity carried to an extreme becomes elegance</h2>
                <h3>Your home should be a story of who you are, and be a collection of what you love</h3>
                <a href="/all-products">Explore Now</a>
            </div>
        </section>

        <section class="bestSellingProductSection">
            <h1 class="sectionTitle">Best Selling Products</h1>
            <div class="productContainer">
                <div class="productCard">
                    <img src={table1} class="productImg" alt="" />
                    <p class="productName">Tables</p>
                </div>
                <div class="productCard">
                    <img src={bed1} class="productImg" alt="" />
                    <p class="productName">Beds</p>
                </div>
                <div class="productCard">
                    <img src={sofa1} class="productImg" alt="" />
                    <p class="productName">Sofas</p>
                </div>
                <div class="productCard">
                    <img src={light1} class="productImg" alt="" />
                    <p class="productName">Lights</p>
                </div>
            </div>
        </section>

        <section class="midSection">
            <div class="sectionItemContainer">
                <img src={poster1} class="sectionBg" alt="" />
                <div class="sectionInfo">
                    <h1 class="title">Furniture that tells a story</h1>
                    <p class="infoSec">Designs inspired by the new generation with a classic touch</p>
                </div>
            </div>
        </section>

        <section class="imageMidSection">   
            <div class="imageCollage">
                <div class="imageCollection">
                    <img src={poster4} class="collageImg" alt="" />
                    <img src={poster3} class="collageImg" alt="" />
                    <img src={poster5} class="collageImg" alt="" />
                </div>
            </div>
        </section>

        <section class="reviewSection">
            <h1 class="sectionTitle">What our <span>customers</span> say about us</h1>
            <div class="reviewContainer">
                <div class="reviewCard">
                    <div class="userDP" data-rating="4.9">
                        <img src={user1} alt="" />
                    </div>
                    <h2 class="reviewTitle">The quality exceeds my expectations</h2>
                    <p class="review">I bought the set after searching and searching. I love them both and they are beautiful. I highly recommend purchasing them.</p>
                </div>  
                <div class="reviewCard">
                    <div class="userDP" data-rating="4.7">
                        <img src={user2} alt="" />
                    </div>
                    <h2 class="reviewTitle">I love this product and recommend it</h2>
                    <p class="review">This is a very comfortable couch and the recliners are comfortable also.</p>
                </div>  
                <div class="reviewCard">
                    <div class="userDP" data-rating="4.8">
                        <img src={user3} alt="" />
                    </div>
                    <h2 class="reviewTitle">Great table. Would purchase again!</h2>
                    <p class="review">Love this table! Color brown with gray undertones so works well with many color pallets.</p>
                </div>
                <div class="reviewCard">
                    <div class="userDP" data-rating="4.6">
                        <img src={user4} alt="" />
                    </div>
                    <h2 class="reviewTitle">I love it... Great products for best prices</h2>
                    <p class="review">I love the color of the sofa. Also it's very sturdy. The reclining mechanism is very easy to maneuver. Good quality.</p>
                </div>    
            </div>
        </section>

        <section class="endSection">
            <div class="sectionItemContainer">
            <img src={poster6} class="sectionBg" alt="" />
                <div class="sectionInfo">
                    <h1 class="title">Explore Now</h1>
                    <p class="infoSec">Designs inspired by the new generation with a classic touch</p>
                </div>
            </div>
        </section>
        <Footer />
    </div>
  )
}

export default Homepage