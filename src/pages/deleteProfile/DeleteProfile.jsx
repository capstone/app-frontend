import React from 'react'
import { useNavigate } from 'react-router-dom';
import './deleteProfile.css'
import closePic from './close.png'
import axios from "../../api/axios";

const EditProfile = ({onClose, visible}) => {
    const navigate = useNavigate();
    const token = localStorage.getItem('token')
          const tokenWithoutQuotes = token.replace(/"/g, '');

    const handleOnClose = (e) => {
        if(e.target.id ==='deleteProfileContainer')
            onClose()
      }

    const handleDeleteProfile = async () => {
        try {
          console.log(tokenWithoutQuotes)      
          const response = await axios.delete('/profile', {
            headers: {
              Authorization: `Bearer ${tokenWithoutQuotes}`,
            },
          });
      
          console.log(response);
        } catch (error) {
          if (error.response) {
            console.log(error.response.data);
            console.log(error.response.status);
            console.log(error.response.headers);
            if (tokenWithoutQuotes) {
                localStorage.clear();
              } 
            navigate('/homepage');
          } else if (error.request) {
            console.log(error.request);
          } else {
            console.log('Error', error.message);
          }
        }
      };
    
    if (!visible) return null

  return (
    <div className='deleteProfilePage' id='deleteProfileContainer' onClick={handleOnClose}>
      <div className='editProfilePopUp'>
            <img src={closePic} alt="" className="closePic" onClick={onClose} />
            <h3 className="reportHeading">
                Are you sure you want to delete your profile?
            </h3>
            <button className='btn reportBtn' onClick={handleDeleteProfile}>Yes, I'm sure</button>
        </div>
    </div>
  )
}

export default EditProfile
