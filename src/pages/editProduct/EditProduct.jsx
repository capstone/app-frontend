import { useParams } from "react-router-dom";
import React, { useState } from 'react';
import "./editProduct.css";
import "../../component/carousel/carousel.css"
import Footer from '../../component/footer/Footer'
import Header from '../../component/header/Header'
import useFetchData from "../../hooks/useFetchData";
import Feedback from "../feedback/Feedback";
import Carousel from "../../component/carousel/Carousel";
import { useEffect } from "react";
import axios from "../../api/dataAxios";
import { useNavigate } from "react-router-dom";
import { Navigate } from "react-router-dom";

const token = localStorage.getItem('token');

function EditProduct() {
    let { productId } = useParams();
    const [data, setData] = useState(null);
    const [name, setName] = useState({inputVal: ""});
    const [price, setPrice] = useState({inputVal: ""});
    const [condition, setCondition] = useState({inputVal: ""});
    const [description, setDescription] = useState({inputVal: ""});
    const [marketAvailableDate, setMarketAvailableDate] = useState({inputVal: ""});
    const [location, setLocation] = useState({inputVal: ""});
    const [category, setCategory] = useState({inputVal: ""});
    const [status, setStatus] = useState(false);
    const [cancel, setCancel] = useState(false);
    const [removed, setRemoved] = useState([]);
    const [imageName, setImageName]=useState([]);
    const [files, setFiles] = useState([]);
    const navigate = useNavigate();
    const activeImageSlide = 0;
    const dateParser = (DATE) => {
        var datee = new Date(DATE);
        var getYear = datee.toLocaleString("default", { year: "numeric" });
        var getMonth = datee.toLocaleString("default", { month: "2-digit" });
        var getDay = datee.toLocaleString("default", { day: "2-digit" });
        return getYear + "-" + getMonth + "-" + getDay;
    }
    useEffect(() => {
      axios
        .get("products", {
          params: {
            id: productId,
          },
        })
        .then((e) => {
          let DATA = e.data.product;
        //   console.log(DATA);
          DATA.images = [];
          setData(DATA);
          axios
            .get("list_files", {
              params: {
                product_id: e.data.product.id,
                user_id: e.data.product.seller_id,
              },
            })
            .then((images) => {
              const imagess = [];
              setImageName(images.data);
              {
                images.data.map((val) => {
                  // console.log("in usefetchdata val = ", val)
                  const filePath =
                    "http://127.0.0.1:8080/get_file?product_id=" +
                    e.data.product.id +
                    "&file_name=" +
                    val +
                    "&user_id=" +
                    e.data.product.seller_id;
                  // console.log(filePath)
                  imagess.push(filePath);
                });
              }
              DATA.images = imagess;
              if (e.status === 200) {
                // console.log(DATA);
                setData(DATA);
                setStatus(true);
                setName({inputVal: DATA?.name});
                setPrice({inputVal: DATA?.price});
                setCondition({inputVal: DATA?.condition});
                setDescription({inputVal: DATA?.description});
                setMarketAvailableDate({inputVal: DATA?.market_available_date});
                setLocation({inputVal: DATA?.location});
                setCategory({inputVal: DATA?.category});
              } else {
                setStatus(false);
                setData(null);
              }
            });
        });
    }, [productId]);

    // const ImageSlider = productImages.forEach((item, i)=> {
    //     item.addEventListener('click', () => {
    //         productImages[activeImageSlide].classList.remove('active');
    //         item.classList.add('active');
    //         productImageSlide.style.backgroundImage = `url('${item.src}')`;
    //         activeImageSlide = i;
    //     })
    // })

  const [popup, setPopup] = useState(false)
  const handleOnClose = () => {
    setPopup(false)
  }

  const handleFileUpload = (event) => {
    setFiles([...files, ...event.target.files] )
  }; 

  const handleSubmit = async (event) => {
    event.preventDefault();
    const token = localStorage.getItem('token');

    try {
      console.log("Sending: ");
      console.log(
        JSON.stringify({
        name:name,
        description:description,
        price:price,
        category:category,
        condition:condition,
        location:location,
        market_available_date: marketAvailableDate
        })
      );
      var tokenWithoutQuotes = "hello";
      if(token!=null){
        tokenWithoutQuotes = token.replace(/"/g, '');
      }
      // const tokenWithoutQuotes = token.replace(/"/g, '');
      const response = await axios.put('/product_update', {
        name:name.inputVal,
        id: productId,
        description:description.inputVal,
        price:price.inputVal,
        category:category.inputVal,
        condition:condition.inputVal,
        location:location.inputVal,
        market_available_date: dateParser(marketAvailableDate.inputVal)
      },
      {
        headers: {
          "Authorization": `Bearer ${tokenWithoutQuotes}`,
          "Content-Type": "application/json"
        }
      });
      removed.map(async (index)=>{
        const responseDel = await axios.delete('/delete_file',
        {
          params: {product_id: productId,
          file_name:imageName[index],},
          headers: {
            "Authorization": `Bearer ${tokenWithoutQuotes}`,
            "Content-Type": "application/json"
          }
        }
        );
      })
      let formData = new FormData();
      console.log(files)
      for (let i = 0; i < files.length; i++) {
        formData.append("file", files[i])
      }
      console.log(formData)
      axios.post('upload_files', formData, {
          headers: {
            "Authorization": `Bearer ${tokenWithoutQuotes}`,
            "Content-Type": 'multipart/form-data'
          },
          params:{
            product_id: productId
          }
      });
      

      alert('Product updated successfully!');
      navigate("/seller");

    } catch (err) {
      if (err.response) {
        console.log(err.response.data);
      } else if (err.message) {
        console.log(err.message);
      } else {
        console.log("No response from the server");
      }
    }
  };

  return (
    <>
    {cancel && <Navigate to="/seller" replace={true} />}
    <div>
        <Header />
        {status && <div>
        <section className="productDetails">
            {/* <div class="imageSlider" style={{backgroundImage: `url(${data.images[0]})`}}>
                <div class="productImages">
                    <img src={data.images[0]} class="active" alt=""/>
                    <img src={data.images[1]} alt=""/>
                    <img src={data.images[2]} alt=""/>
                    <img src={data.images[3]} alt=""/>
                </div>
            </div> */}
            <div className="carousel">
            <Carousel SRC={data.images} edit={true} setRemoved={setRemoved} removed={removed}/>
            <input type="file" name = "file" id="imagees" onChange={handleFileUpload} multiple='true' />
            </div>
            <div className="details">
                {/* {console.log(data)} */}
                <input class="productBrand" type="text" value={name?.inputVal} onChange={(e) => {setName({inputVal: e.target.value})}} />
                {/* <h2 class="productBrand">{data?.name}</h2> */}
                
                <label class="productPrice">Price</label>
                <input class="productPrice" type="text" value={price?.inputVal} onChange={(e) => {setPrice({inputVal: e.target.value})}} />
                {/* <p class="productCondition">Condition - {data.condition}</p> */}
                <p class="productCondition">Condition</p>
                <input class="productCondition" type="text" value={condition?.inputVal} onChange={(e) => {setCondition({inputVal: e.target.value})}} />
                <p class="productCondition">Location</p>
                <input class="productCondition" type="text" value={location?.inputVal} onChange={(e) => {setLocation({inputVal: e.target.value})}} />
                <p class="productCondition">Category</p>
                <input class="productCondition" type="text" value={category?.inputVal} onChange={(e) => {setCategory({inputVal: e.target.value})}} />
                
                <p class="marketDate">Market available date:</p>
                <input type="date" class="marketDate" id="marketDate" placeholder="Available On" value={dateParser(marketAvailableDate?.inputVal)} onChange={(event) => setMarketAvailableDate({inputVal: event.target.value})}/>
                <p class="uploadDate">Uploaded On - {data?.created_date}</p>

                {token && <div className="btnGroup">
                    <button class="contactSellerBtn">Contact Seller</button>
                    <button class="reportSellerBtn" onClick={() => setPopup(true)}>Report Product/Seller</button>
                    <Feedback onClose={handleOnClose} visible={popup}/>
                </div>}
                {!token && <div className="btnGroup">
                    <button class="contactSellerBtn" onClick={() => navigate("/login")}>Login/Register to see Seller Contact Information</button>
                </div>}
            </div>
        </section>

        <section class="deatilsDescription">
            <h2 class="heading">description</h2>
            <textarea class="description" type="text" value={description?.inputVal} onChange={(e) => {setDescription({inputVal: e.target.value})}} />
            {/* <table className="table">
              <tbody id="table-body">
              {data.Description.map((val, key) => {
                return (
                  <tr key={key}>
                    <td className="leftColumn">{val.key}</td>
                    <td className="rightColumn">{val.value}</td>
                  </tr>
                )
              })}
              </tbody>
            </table> */}
        </section>
        <div className="ProductInfo">
        <button className="View" onClick={handleSubmit}>
          Save
        </button>
        <button className="Edit" onClick={()=>{setCancel(true)}}>
          Cancel
        </button>
        </div>
        </div>
        }
        <Footer />
    </div>
    </>
  )
}

export default EditProduct
