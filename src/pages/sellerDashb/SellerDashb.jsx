import React from 'react'
import "./sellerDashb.css";
import seller_logo from './logo2.png'
import useFetchProdBySeller from "../../hooks/useFetchProdBySeller";
import ProductCard from "../../component/productCard/ProductCard";
import {useNavigate} from 'react-router-dom'
import no_product from './no-products.png'

const SellerDashb = () => {
    const navigate = useNavigate();

    const navigateToAddProduct = () => {
        navigate('/add-product');
    };

    const token = localStorage.getItem('token');
    var tokenWithoutQuotes = "";
    if (token!=null){
        tokenWithoutQuotes = token.replace(/"/g, '');
    }

    const {dataProduct, status} = useFetchProdBySeller(tokenWithoutQuotes);
    
  return (
    <div className='seller'>
        <img src={seller_logo} class="seller_logo" alt="" />
      <div className={"productListing"}>
            <div class="addProduct">
                <p class="addProductTitle">Add Items to Sell</p>
                <button class="btn" onClick={navigateToAddProduct}>Add Item</button>
            </div>
            {dataProduct && <div className="Products">
                  {dataProduct.map((val, key) => {
                    return (
                      <ProductCard key={key}
                        seller_id={val.seller_id}
                        price = {val.price}
                        name = {val.name}
                        date={val.date}
                        id={val.id}
                        edit={true}
                        deletePrd={true}
                        markAsSold={true}
                        is_sold={val.is_sold}
                      />
                    )
                  })}
                </div>}
            {!dataProduct && <img src={no_product} class="noProductImage" alt="" />}
        </div>
    </div>
  )
}

export default SellerDashb
