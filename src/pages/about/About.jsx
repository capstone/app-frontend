import React from 'react'
import Footer from '../../component/footer/Footer'
import Header from '../../component/header/Header'
import './about.css'
import {useNavigate} from 'react-router-dom'

const About = () => {
    const navigate = useNavigate();

    const navigateToContactPage = () => {
      navigate('/contact');
    };  
    return (
        <div className='aboutPage'>
            <Header />

            <div className='about'>
                <p class="aboutheading">About On The Block</p>
                <p class="desc">Welcome to our online platform for second-hand goods trading designed specifically for college students! We believe that buying and selling second-hand items is not only a great way to save money, but also an environmentally friendly way to shop.</p>

                <p class="desc">Our platform makes it easy for students to buy and sell second-hand items within their college community. Whether you're looking for textbooks, furniture, electronics, or clothing, our platform provides a safe and reliable space for you to browse, buy, and sell.</p>

                <p class="desc">We understand the challenges that college students face when it comes to buying and selling goods. Limited budgets, busy schedules, and the hassle of moving from place to place can all make it difficult to purchase and sell the items you need. That's why we've created a platform that's tailored to the needs of college students.</p>

                <p class="desc">Our platform is easy to use and designed with students in mind. You can browse items by category, search for specific items, or filter by price range, location, and more. We also offer a secure payment system, so you can buy and sell with confidence.</p>

                <p class="desc">In addition to providing a convenient and reliable marketplace for college students, we're also committed to sustainability. By buying and selling second-hand items, you're helping to reduce waste and conserve resources. We believe that everyone can make a positive impact on the environment, and we're proud to support students who are doing just that.</p>

                <p class="desc">Thank you for choosing our platform for your second-hand goods trading needs. We're excited to help you save money, simplify your life, and make a difference in the world.</p>

                <button class="btn" id="applyBtn" onClick={navigateToContactPage}>
                    Contact Us
                </button>
            </div>
        
        <Footer />
    </div>
  )
}

export default About
