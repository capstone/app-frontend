import React, { useEffect } from "react";
import Footer from "../../component/footer/Footer";
import Header from "../../component/header/Header";
import ProductCard from "../../component/productCard/ProductCard";
import useFetchAllProd from "../../hooks/useFetchAllProd";
import "./allProducts.css";
import { useState } from "react";
import axios from "../../api/dataAxios";
import Filter from "../../component/filterCard/Filter";
import no_product from './no-products.png'


const AllProducts = () => {
  const [visData, setVisData] = useState([]);
  const [data, setData] = useState([]);
  const [status, setStatus] = useState(false);
  const [categories, setCategories] = useState([]);
  const [location, setLocation] = useState([]);
  const [priceRange, setPriceRange] = useState(0);
  var catList = ["All"]
  var locList = ["All"]
  var maxPrice = 0;
  useEffect(()=>{
    if(data!==null){
        console.log("here")
        data.map((e)=>{
          console.log(e)
          catList.push(e.category)
          locList.push(e.location)
          if(maxPrice<e.Price){
            maxPrice = e.Price;
          }
        })
        setCategories(catList);
        setLocation(locList);
        setPriceRange(maxPrice);
        console.log(locList)
      }
  },[data])
  useEffect(() => {
    axios.get("products").then((e) => {
      let DATA = e.data;
      DATA.products.map((key, val) => {
        DATA.products[val].images = [];
        axios
          .get("list_files", {
            params: {
              product_id: DATA.products[val].id,
              user_id: DATA.products[val].seller_id,
            },
          })
          .then((images) => {
            const imagess = [];
            {
              images.data.map((fname) => {
                const filePath =
                  "http://127.0.0.1:8080/get_file?product_id=" +
                  DATA.products[val].id +
                  "&file_name=" +
                  fname +
                  "&user_id=" +
                  DATA.products[val].seller_id;
                imagess.push(filePath);
              });
            }
            DATA.products[val].thumbnail = imagess[0];
            DATA.products[val].images = imagess;
          });
      });
      if (e.status === 200) {
        setStatus(true);
        setData(DATA.products);
        setVisData(DATA.products)
      } else {
        setStatus(false);
        setData([]);
      }
    });
  }, []);
  return (
    <div className="allProducts">
      <Header data={data} allProd={true} />
      <Filter data={data} setVisData={setVisData}/>
      {!status && <img src={no_product} class="noProductImage"/>}
      {status && (
        <div className="Products">
          {visData.map((val, key) => {
            return (
              <ProductCard
                key={key}
                seller_id={val.seller_id}
                price={val.price}
                name={val.name}
                date={val.market_available_date.substring(0, 10)}
                id={val.id}
                edit={false}
                is_sold={val.is_sold}
              />
            );
          })}
        </div>
      )}
      <Footer />
    </div>
  );
};

export default AllProducts;
