import './userProfile.css'
import React, { useState, useEffect } from 'react';
import axios from '../../api/axios';
import Footer from '../../component/footer/Footer'
import Header from '../../component/header/Header'
import userProfileInitialImage from './userProfileImage.png'
import EditProfile from '../editProfile/EditProfile';
import DeleteProfile from '../deleteProfile/DeleteProfile';

const UserProfile = () =>{
    const [userDetails, setUserDetails] = useState(null);
    const [file, setFile] = useState(null);
    const [profileImagePath, setProfileImagePath] = useState(null);

    const [popupEditProfile, setPopupEditProfile] = useState(false)
    const handleOnCloseEditProfile = () => {
      setPopupEditProfile(false)
    } 

    const [popupDeleteProfile, setPopupDeleteProfile] = useState(false)
    const handleOnCloseDeleteProfile = () => {
      setPopupDeleteProfile(false)
    } 

    const fetchUserProfileImage = async () => {
      const token = localStorage.getItem('token')
      const tokenWithoutQuotes = token.replace(/"/g, '');

      const response = await axios.get("/user/profile-image", {
        headers: {
          Authorization: `Bearer ${tokenWithoutQuotes}`,
        },
        responseType: 'blob'
      })

      const blob = new Blob([response.data], { type: 'image/png' });
      const imageUrl = URL.createObjectURL(blob);

      setProfileImagePath(imageUrl);
    };

    const handleFileChange = (event) => {
      setFile(event.target.files[0]);
    };

    const handleSubmit = async (event) => {
      event.preventDefault();
      const token = localStorage.getItem('token')
      const tokenWithoutQuotes = token.replace(/"/g, '');
      const formData = new FormData();
      console.log(tokenWithoutQuotes)
      formData.append('profile_image', file);
      console.log(formData.get('profile_image'))
      try {
        const response = await axios.post('/user/profile-image', formData, {
          headers: {
            'Content-Type': 'multipart/form-data',
            'Authorization': `Bearer ${tokenWithoutQuotes}`
          }
        });
        console.log(response.data);
        console.log(response.data.profile_image);
        fetchUserProfileImage();
      } catch (error) {
        console.error(error);
        // Handle error
      }
    };

    useEffect(() => {
        fetchUserProfileImage();
        const token = localStorage.getItem('token');
        const tokenWithoutQuotes = token.replace(/"/g, '');
        const username = localStorage.getItem('userName');
        const userNameWithoutQuotes = username.replace(/"/g, '');
        console.log(token)
        console.log(tokenWithoutQuotes)
        console.log(userNameWithoutQuotes)
        axios.get('/profile', {
          params: {
            username: userNameWithoutQuotes
          },
          headers: {
            Authorization: `Bearer ${tokenWithoutQuotes}`
          }
        })
        .then(response => {
          setUserDetails(response.data);
          console.log(userDetails)
        })
        .catch(error => {
          console.error(error);
        });
      }, []);

      if (!userDetails) {
        return <div>Loading...</div>;
      }

  return (
    <div>
      <Header />
      <div className='userProfilePage' id='userProfileContainer'>
        <div className='userProfilePopUp'>
            <h3 className="userProfileHeading">
                My Profile
            </h3>
            <table className='userProfileTable'>
              <tr className='userProfiletr'>
                <td className='userProfiletd'>User Name</td>
                <td className='userProfiletd'>:  {userDetails.username}</td>
              </tr>
              <tr>
                <td className='userProfiletd'>Email</td>
                <td className='userProfiletd'>:  {userDetails.email}</td>
              </tr>
              <tr>
                <td className='userProfiletd'>Phone</td>
                <td className='userProfiletd'>:  {userDetails.phone}</td>
              </tr>
              <tr>
                <td className='userProfiletd'>College</td>
                <td className='userProfiletd'>:  {userDetails.college}</td>
              </tr>
              <tr>
                <td className='userProfiletd'>Buyer Rating</td>
                <td className='userProfiletd'>:  {userDetails.avg_buyer_rating ? userDetails.avg_buyer_rating : '0'}</td>
              </tr>
              <tr>
                <td className='userProfiletd'>Seller Rating</td>
                <td className='userProfiletd'>:  {userDetails.avg_seller_rating ? Math.round(userDetails.avg_seller_rating * 100) / 100 : 0}</td>
              </tr>
            </table>
            <div className='profileBtns'>
              <button className="btn" type='submit' onClick={() => setPopupEditProfile(true)}>Edit Profile</button> 
              <EditProfile onClose={handleOnCloseEditProfile} visible={popupEditProfile}/>
              <button className='btn' id='profileDeleteBtn' type='submit' onClick={() => setPopupDeleteProfile(true)}>Delete Profile</button>
              <DeleteProfile onClose={handleOnCloseDeleteProfile} visible={popupDeleteProfile}/>
            </div>
        </div>
        
        <div className='userProfileImageContainer'>
          {profileImagePath ? (<img src={profileImagePath} alt="User Profile" className='userProfileInitialImage'/>): (<img src={userProfileInitialImage} alt='' className='userProfileInitialImage' />)}
          <div>
            <form onSubmit={handleSubmit}>
              <input type="file" className='profileImageFile' onChange={handleFileChange} />
              <button className="btn" id="uploadPictureBtn" type="submit">Upload New Profile Image</button>
            </form>
          </div>
        </div>
      </div>
      <Footer />
    </div>
    
  )
}

export default UserProfile
