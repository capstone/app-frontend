import "./verification.css";
import reg_logo from "./logo1.png";
import axios from "../../api/axios";
import { userRef, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useLocation } from "react-router-dom";

//End point in backend API
const VERIFICATION_URL = "/verify-code";
const TwoFA_email_URL = "2fa-email";
const TwoFA_phone_URL = "2fa-phone";



const Verification = () => {
  const [showInput, setShowInput] = useState(false);
  const [selectedOption, setSelectedOption] = useState('');
  const location = useLocation();
  const data = location.state;
  const [errorMessage, setErrorMessage] = useState("");

  const handleButtonClick = async () => {
    setShowInput(true);
    if(selectedOption === 'email') {
        try{
            console.log("Sending: ");
            console.log(
                JSON.stringify({
                    email: data.email,
                })
            );
            const response = await axios.post(
                TwoFA_email_URL,
                {
                    email:data.email,
                },
                {
                header: { "Content-Type": "application/json" },
                withCredentials: false,
                }
            );

        }
        catch(err) {
            if (!err.message) {
                console.log("No response from the server");
              } else {
                console.log(err.message);
              }
        }

    } else if (selectedOption === 'phone') {
        try{
            console.log("Sending: ");
            console.log(
                JSON.stringify({
                    phone_number: data.phone,
                })
            );
            const response = await axios.post(
                TwoFA_phone_URL,
                {
                    phone_number:data.phone,
                },
                {
                header: { "Content-Type": "application/json" },
                withCredentials: false,
                }
            );

        }
        catch(err) {
            if (!err.message) {
                console.log("No response from the server");
              } else {
                console.log(err.message);
              }
        }
    }
  }

  const handleOptionChange = (event) => {
    setSelectedOption(event.target.value);
  }


  const [otp, setOTP] = useState("");
  const [enable, setEnable] = useState(true);
  
  const navigate = useNavigate();
  const [focused1, setFocused1] = useState(false);

  const handleFocus1 = (e) => {
    setFocused1(true);
  };
  
  const handleSubmit = async (e) => {
    setEnable(false);
    e.preventDefault();
    if(selectedOption === 'email') {
        try {
            console.log("Sending: ");
            console.log(
              JSON.stringify({
                email:data.email,
                phone_number: "",
                code:otp,
              })
            );
            const response = await axios.post(
              VERIFICATION_URL,
              {
                email:data.email,
                phone_number: "",
                code:otp,
              },
              {
                header: { "Content-Type": "application/json" },
                withCredentials: false,
              }
            );
            // The API call was successful
            console.log("Response Data:", response.data);
            setEnable(true);
            navigate("/login", { replace: true });
          } catch (err) {
            setEnable(true);
            if (!err.message) {
              console.log("No response from the server");
              setErrorMessage("No response from the server");
            } else {
              console.log(err.message);
              setErrorMessage(err.message);
            }
          }
    } else if (selectedOption === 'phone') {
        try {
            console.log("Sending: ");
            console.log(
              JSON.stringify({
                email: "",
                phone_number: data.phone,
                code:otp,
              })
            );
            const response = await axios.post(
              VERIFICATION_URL,
              {
                email:"",
                phone_number: data.phone,
                code:otp,
              },
              {
                header: { "Content-Type": "application/json" },
                withCredentials: false,
              }
            );
            // The API call was successful
            console.log("Response Data:", response.data);
            setEnable(true);
            navigate("/login", { replace: true });
            
            
          } catch (err) {
            setEnable(true);
            if (!err.message) {
              console.log("No response from the server");
              setErrorMessage("No response from the server");
            } else {
              console.log(err.message);
              setErrorMessage(err.message);
            }
          }
    }
  };

  return (
    <div className="register">
      <div className="register-card">
        <div className="left">
          <img src={reg_logo} alt="" className="reg-logo" />
          <p className="reg-logo-tagline">
            One-stop for college students. Everything your home deserves at
            affordable prices!
          </p>
          <span className="reg-logo-text">Already have an account?</span>
          <button
            className="register-button"
            onClick={() => navigate("/login")}
          >
            Login
          </button>
        </div>
        <div className="right">
          <h1 className="register-header">Verify your account</h1>
          <div>
            <h5>Please choose if you want the verification code sent to your Email or Phone Number</h5>
            <form className='registerForm' onSubmit={handleSubmit}>
                
                <div>
                <input type="radio" name="option" value="phone" onChange={handleOptionChange} disabled={showInput} /> By Phone
                </div>
                <div>
                <input type="radio" name="option" value="email" onChange={handleOptionChange} disabled={showInput} /> By Email
                </div>
                
                <button className="login-button" onClick={handleButtonClick} disabled={!selectedOption || showInput}>Request Verification Code</button>
                {showInput && (
                <div>
                    <h5>Please enter the verification code received. This code will expire in 10 min!</h5>
                    <input
                        type="text"
                        placeholder="OTP Verification"
                        onChange={(e) => setOTP(e.target.value)}
                        onBlur={handleFocus1}
                        focused1={focused1.toString()}
                        pattern="^^[a-zA-Z0-9]{6}$"
                        required
                    />
                    {errorMessage && <p>{errorMessage}</p>}
                    <span id="nameError"><p>Please enter a valid OTP that you received on your email or phone number.</p></span>
                    {enable && <button className="login-button">Register</button>}
                </div>
                )}
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Verification;
