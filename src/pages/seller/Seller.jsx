import "./seller.css";
import React, {useState} from 'react';
import seller_logo from './logo2.png'
import {useNavigate} from 'react-router-dom'
import { useEffect } from 'react'
import axios from "../../api/axios";
import useFetchProdBySeller from "../../hooks/useFetchProdBySeller";
import ProductCard from "../../component/productCard/ProductCard";

const REGISTER_URL = "/register";

const Seller = () => {
  const token = localStorage.getItem('token');
  var tokenWithoutQuotes = "";
  if (token!=null){
    tokenWithoutQuotes = token.replace(/"/g, '');
  }

  const [visible, setVisible] = useState(0);
  // const {dataProduct, status} = useFetchProdBySeller(tokenWithoutQuotes);

  const handleVisible = (e) =>{
    setVisible(e);
  }

  const navigate = useNavigate();

  const navigateToAddProduct = () => {
    navigate('/seller-dashboard');
  };

  useEffect(() => {
    window.scrollTo(0, 0)
  }, [])

  const [user, setUser] = useState("");
  const [pwd, setPwd] = useState("");
  const [email, setEmail] = useState("");
  const [college, setCollege] = useState("");
  const [enable, setEnable] = useState(true);
  
  const [focused1, setFocused1] = useState(false);
  const [focused2, setFocused2] = useState(false);
  const [focused3, setFocused3] = useState(false);
  const [focused4, setFocused4] = useState(false);

  const handleFocus1 = (e) => {
    setFocused1(true);
  };
  const handleFocus2 = (e) => {
    setFocused2(true);
  };
  const handleFocus3 = (e) => {
    setFocused3(true);
  };
  const handleFocus4 = (e) => {
    setFocused4(true);
  };

  const handleSubmit = async (e) => {
    setEnable(false);
    e.preventDefault();
    try {
      const response = await axios.post(
        REGISTER_URL,
        {
          username: user,
          password: pwd,
          email: email,
          college: college,
        },
        {
          header: { "Content-Type": "application/json" },
          withCredentials: false,
        }
      );
    } catch (err) {
      if (!err.message) {
        console.log("No response from the server");
      } else {
        console.log(err.message);
      }
    }
    setEnable(true);
    navigate("/add-product", { replace: true });
  };

  return (
    <div className="seller">
        <img src={seller_logo} class="seller_logo" alt="" />
        <div className={visible===0 ? "becomeSeller" : "becomeSeller hide"}>
            <p class="heading">Seller Dashboard</p>
            <p class="desc">To become a seller on our platform, you must first register for an account. During the registration process, you will be required to provide certain information, including your name, address, and other information. You agree to provide accurate, complete, and up-to-date information during the registration process and to keep your account information up-to-date at all times.</p>

            <p class="desc">As a seller on the Platform, you may list products for sale on the Platform. When listing products, you agree to provide accurate, complete, and up-to-date information about your products, including product descriptions, prices, and images. You also agree to comply with all applicable laws and regulations in connection with your product listings.</p>

            <p class="desc">When a buyer purchases a product from you through the Platform, you agree to fulfill the order in a timely and professional manner. You agree to comply with all applicable laws and regulations in connection with the sale and delivery of your products.</p>

            <p class="desc">You will receive payments for your sales on the Platform through the payment processing system provided by the Platform. You agree to pay all fees and charges associated with the use of the payment processing system.</p>

            <p class="desc">In the event of a dispute between you and a buyer, you agree to cooperate with the platform in resolving the dispute. You agree to comply with any decision made by the platform in connection with the dispute.</p>

            <button class="btn" id="applyBtn" onClick={navigateToAddProduct} >
                Go to your dashboard
            </button>
        </div>
        <div className={visible===1 ? "applyForm" : "applyForm hide"} onSubmit={handleSubmit}>
            <input 
            type="text" 
            id="businessName" 
            placeholder="Username" 
            onChange={(e) => setUser(e.target.value)}
            onBlur={handleFocus1}
            focused1={focused1.toString()}
            pattern="^[A-Za-z0-9]{3,30}$"
            required />

            <span id="nameError"><p>Please enter a valid username between 3 to 30 characters without any special characters.</p></span>

            <input 
            type="email" 
            id="businessAdd" 
            placeholder="Email"
            onChange={(e) => setEmail(e.target.value)}
            onBlur={handleFocus2}
            focused2={focused2.toString()}
            pattern="^[A-Za-z0-9._%+-]+@vt\.edu$"
            required />

            <span id="emailError"><p>Please enter a valid VT email id.</p></span>

            <input 
            type="password" 
            id="about" 
            placeholder="Password"
            onChange={(e) => setPwd(e.target.value)}
            onBlur={handleFocus3}
            focused3={focused3.toString()}
            pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*])[A-Za-z\d!@#$%^&*]{8,}$"
            required />

            <span id="passwordError"><p>Please enter a valid password. It should be atleast 8 characters long with 1 Uppercase letter, 1 digit and 1 special character.</p></span>

            <input 
            type="text" 
            id="number" 
            placeholder="College Name" 
            onChange={(e) => setCollege(e.target.value)}
            onBlur={handleFocus4}
            focused4={focused4.toString()}
            pattern="^(VT|Virginia Tech|VTech)$"
            required/>

            <span id="collegeError"><p>Please enter a valid college name.</p></span>

            <input type="text" id="address" placeholder="Address" />
            <input type="text" id="street" placeholder="Street" />
            <div class="threeInputContainer">
                <input type="text" id="city" placeholder="City" />
                <input type="text" id="state" placeholder="State" />
                <input type="text" id="pincode" placeholder="Pincode" />
            </div>
            <input type="text" id="landmark" placeholder="Landmark" />

            <br />
            <br />
            <input type="checkbox" class="checkbox" checked id="termsAndConditions" />
            <label for="termsAndConditions">Agree to our <a href="">Terms and Conditions</a></label>
            <br />
            <input type="checkbox" class="checkbox" checked id="legitInfo" />
            <label for="legitInfo">All information is correct to the best of my ability</label>
            <br />
            <br />
            {enable && <button class="submitBtn" id="applyFormBtn" onClick={()=>handleVisible(2)}>Apply</button>}
        </div>
        {/* <div className={visible===2 ? "productListing" : "productListing hide"}>
            <div class="addProduct">
                <p class="addProductTitle">Add Items to Sell</p>
                <button class="btn" onClick={navigateToAddProduct}>Add Item</button>
            </div>
            {dataProduct && <div className="Products">
                  {dataProduct.map((val, key) => {
                    return (
                      <ProductCard key={key}
                        seller_id={val.seller_id}
                        price = {val.price}
                        name = {val.name}
                        date={val.date}
                        id={val.id}
                        edit={true}
                        deletePrd={true}
                      />
                    )
                  })}
                </div>}
            {!dataProduct && <img src={no_product} class="noProductImage" alt="" />}
        </div> */}
    </div>
  )
}

export default Seller