import "./forgotPassword.css";
// import login_logo from "./logo1.png";
import login_logo from "./logo1.png";
import GoogleLogin from "../../component/auth/GoogleLogin";
import { useAuthState } from "react-firebase-hooks/auth";
import { useNavigate } from "react-router-dom";
import { auth } from "../../utils/firbase";
import { Navigate } from "react-router-dom";
import { useState } from "react";
import axios from "../../api/axios";

const FORGOT_URL = "/forgot-password";

const ForgotPassword = ({}) => {
  const navigate = useNavigate();
  const [email, setEmail] = useState("");
  const [focus, setFocus] = useState(false);
  const [enable, setEnable] = useState(false);
  const [wrongEmail, setWrongEmail] = useState(false);
  const [success, setSuccess] = useState(false);

  const handleSubmit = async (e) => {
    console.log("submit")
    setEnable(false);
    e.preventDefault();
    try {
      const response = await axios.post(
        FORGOT_URL,
        {
          email: email,
        },
        {
          header: { "Content-Type": "application/json" },
          withCredentials: false,
        }
      );
      console.log(response)
      if (response.status == 200) {
        setSuccess(true);
        setWrongEmail(false);
        console.log(response)
      }
    } catch (err) {
      if (!err.message) {
        console.log("No response from the server");
      } else {
        console.log("sent successfully")
        console.log(err.message);
        setWrongEmail(true);
        setSuccess(false);
      }
    }
    setEnable(true);
  };

  const handleFocus = (e) => {
    setFocus(true);
  };

  return (
    <>
      <div className="login">
        <div className="card">
          <div className="left">
            <img src={login_logo} alt="" className="login_logo" />
            <p className="logo-tagline">
              One-stop for college students. Everything your home deserves at
              affordable prices!
            </p>
            <span className="logo-text">Don't have an account?</span>
            <button
              className="register-button"
              onClick={() => navigate("/register")}
            >
              Register
            </button>
          </div>
        </div>
        <div className="right">
          {wrongEmail && <div className="error">Error: User not found</div>}
          {success && <div className="success">Reset link has been sent to you registered Email address</div>}
          <input
            type="text"
            placeholder="Email"
            required
            onChange={(e) => setEmail(e.target.value)}
            pattern="^[A-Za-z0-9._%+-]+@vt\.edu$"
            focus={focus.toString()}
            onBlur={handleFocus}
          />
          <span id="emailError">
            <p>Please enter a valid VT email id.</p>
          </span>
          <button className="login-button" onClickCapture={handleSubmit}>
            Submit
          </button>
        </div>
      </div>
    </>
  );
};

export default ForgotPassword;
