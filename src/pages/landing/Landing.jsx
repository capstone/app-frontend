import "./landing.css";
import Navbar from "../../component/navbar/Navbar";
import ProductCard from "../../component/productCard/ProductCard";
import SearchBar from "../../component/searchBar/SearchBar";
import Filter from "../../component/filterCard/Filter";
import { useAuthState } from "react-firebase-hooks/auth";
import { auth } from "../../utils/firbase";
import { Navigate } from "react-router-dom";
import useFetchData from "../../hooks/useFetchData";

function Landing({ token, userName }) {
  const [user, loading] = useAuthState(auth);
  const {data, status} = useFetchData("");
  return (
    <>
      {!token && <Navigate to="/login" replace={true} />}
      <div className="landing" id="MainPage">
        <Navbar />
        <SearchBar />
        <Filter />
        {status && <div className="Products">
          {data.map((val, key) => {
            return (
              <ProductCard key={key}
                imgUrl={val.images[0]}
                price = {val.price}
                name = {val.name}
                date={val.date}
                id={val.id}
              />
            )
          })}
        </div>}
        {!status &&
        <h1>
          Loading...
        </h1>
        }
      </div>
    </>
  );
}

export default Landing;
