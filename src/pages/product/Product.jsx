import { useParams } from "react-router-dom";
import React, { useState } from 'react';
import "./product.css";
import "../../component/carousel/carousel.css"
import Footer from '../../component/footer/Footer'
import Header from '../../component/header/Header'
import useFetchData from "../../hooks/useFetchData";
import Feedback from "../feedback/Feedback";
import Carousel from "../../component/carousel/Carousel";
import { useEffect } from "react";
import axios from "../../api/dataAxios";
import { useNavigate } from "react-router-dom";
import SellerContact from "../sellerContact/SellerContact";
import { Rating } from 'react-simple-star-rating'

const token = localStorage.getItem('token');

function Product() {
    let { productId } = useParams();
    const [rating, setRating] = useState(null);
    const [data, setData] = useState(null);
    const [status, setStatus] = useState(false);
    const navigate = useNavigate();
    const activeImageSlide = 0;

    const handleRating = (rate) => {
      setRating(rate)
    }

    const token = localStorage.getItem('token');
    var tokenWithoutQuotes = "";
    if (token!=null){
      tokenWithoutQuotes = token.replace(/"/g, '');
    }

    useEffect(()=>{
      if(data!=null && rating!=null){
      axios
      .post("rating", {
        seller_rating:rating,
        username: data.seller_id
      },
      {
        headers: {
          "Authorization": `Bearer ${tokenWithoutQuotes}`,
          "Content-Type": "application/json"
        }
      }).then((e)=>{
        if (e.status === 200) {
          setStatus(true);
        } else {
          setStatus(false);
          }
      })
    }
    },[rating])

    useEffect(() => {
      axios
        .get("products", {
          params: {
            id: productId,
          },
        })
        .then((e) => {
          let DATA = e.data.product;
          console.log(DATA);
          DATA.images = [];
          // setData(DATA);
          axios
            .get("list_files", {
              params: {
                product_id: e.data.product.id,
                user_id: e.data.product.seller_id,
              },
            })
            .then((images) => {
              const imagess = [];
              {
                images.data.map((val) => {
                  const filePath =
                    "http://127.0.0.1:8080/get_file?product_id=" +
                    e.data.product.id +
                    "&file_name=" +
                    val +
                    "&user_id=" +
                    e.data.product.seller_id;
                  imagess.push(filePath);
                });
              }
              DATA.images = imagess;
              if (e.status === 200) {
                console.log(DATA);
                setData(DATA);
                setStatus(true);
              } else {
                setStatus(false);
                setData(null);
              }
            });
        });
    }, [productId]);

    // const ImageSlider = productImages.forEach((item, i)=> {
    //     item.addEventListener('click', () => {
    //         productImages[activeImageSlide].classList.remove('active');
    //         item.classList.add('active');
    //         productImageSlide.style.backgroundImage = `url('${item.src}')`;
    //         activeImageSlide = i;
    //     })
    // })

  const [popup, setPopup] = useState(false)
  const handleOnClose = () => {
    setPopup(false)
  }

  const [popupContactSeller, setContactSellerPopup] = useState(false)
  const handleOnCloseContactSeller = () => {
    setContactSellerPopup(false)
  }

  return (
    <div>
        <Header />
        {status && <div>
        <section className="productDetails">
            {/* <div class="imageSlider" style={{backgroundImage: `url(${data.images[0]})`}}>
                <div class="productImages">
                    <img src={data.images[0]} class="active" alt=""/>
                    <img src={data.images[1]} alt=""/>
                    <img src={data.images[2]} alt=""/>
                    <img src={data.images[3]} alt=""/>
                </div>
            </div> */}
            <Carousel SRC={data.images} />
            <div className="details">
                <h2 class="productBrand">{data?.name}</h2>
                <span class="productShortDescription">Brown Wooden Bed</span>
                <span class="productPrice">Price - $ {data?.price}</span>
                <p class="productCondition">Condition - {data?.condition}</p>
                <p class="productCondition">Category - {data?.category}</p>
                {/* <p class="sellerRating">Seller Rating - {data?.seller_rating || 0}</p> */}
                {/* <p class="sellerID">Seller ID - {data?.seller_id}</p> */}
                {/* <p class="sellerRating">Seller Rating - {data?.seller_rating ? Math.round(data.seller_rating * 100) / 100 : 0 }</p> */}
                <p class="sellerRating">Location - {data?.location}</p>
                <p class="uploadDate">Available on - {data?.market_available_date.substring(0, 10)}</p>

                {token && data?.market_available_date && new Date(data.market_available_date) <= new Date() && <div className="btnGroup">
                    <button class="contactSellerBtn" onClick={() => setContactSellerPopup(true)}>Contact Seller</button>
                    <SellerContact onClose={handleOnCloseContactSeller} visible={popupContactSeller} productId={productId} />
                    <button class="reportSellerBtn" onClick={() => setPopup(true)}>Report Product/Seller</button>
                    <Feedback onClose={handleOnClose} visible={popup} data={data}/>
                </div>}
                {token && new Date(data.market_available_date) > new Date() && <div className="btnGroup">
                    <button class="contactSellerLaterBtn">You will be able to contact seller after the Market available date.</button>
                </div>}
                {!token && <div className="btnGroup">
                    <button class="contactSellerBtn" onClick={() => navigate("/login")}>Login/Register to see Seller Contact Information</button>
                </div>}
                {token && 
                <div className="rating">
                <p className="rateSeller">Rate Seller:</p>
                <Rating className="rating-stars"
                  onClick={handleRating}
                />
                </div>}
            </div>
        </section>

        <section class="deatilsDescription">
            <h2 class="heading">description</h2>
            <p class="description">
            {data?.description}
            </p>
        </section>
        </div>
        }
        <Footer />
    </div>
  )
}

export default Product
